# Mother Miounne [![Matrix](https://img.shields.io/matrix/miounne:etke.cc?logo=matrix&server_fqdn=matrix.org&style=for-the-badge)](https://matrix.to/#/#miounne:etke.cc)[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/etkecc)  [![coverage report](https://gitlab.com/etke.cc/miounne/badges/main/coverage.svg)](https://gitlab.com/etke.cc/miounne/-/commits/main) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/etke.cc/miounne)](https://goreportcard.com/report/gitlab.com/etke.cc/miounne) [![Go Reference](https://pkg.go.dev/badge/gitlab.com/etke.cc/miounne/v2.svg)](https://pkg.go.dev/gitlab.com/etke.cc/miounne/v2)

> [more about that name](https://ffxiv.consolegameswiki.com/wiki/Mother_Miounne)

A bridge between matrix and external services, used as automation backend of [etke.cc](https://etke.cc) service.

## Features

### External services to matrix

* Any HTML form (actually, _any_ HTTP POST form) to matrix room, through bot and/or [appservice-webhooks](https://github.com/turt2live/matrix-appservice-webhooks)
* [buymeacoffee](https://buymeacoffee.com?via=etkecc) API integration (list members, supporters and extras purchases) and notifications
* flexible rate limiter (per item)
* antispam module (honeypot, blocklists and DNS lookups)

### Matrix tools

* [matrix-registration](https://github.com/ZerataX/matrix-registration) token management and notifications in matrix room
* [maubot/echo](https://github.com/maubot/echo)-like latency measurements between matrix servers


## Configuration

done via env vars:

### http server _(optional)_

* **MIOUNNE_LOG_LEVEL** - log level, default: `INFO`
* **MIOUNNE_LOG_IP** - if you want log users IPs, set to `1` to enable
* **MIOUNNE_SERVER_PORT** - HTTP server port, default: 8080
* **MIOUNNE_FREQUENCY** - dealy between background checks (eg: notifications) in minutes, default: `10`

### Buymeacoffee _(optional)_

* **MIOUNNE_BMC_TOKEN** - buymeacoffee API token
* **MIOUNNE_BMC_ROOM** - Matrix Room ID to use as admin room, you should create it yourself and invite Miounne
* **MIOUNNE_BMC_NOTIFY_MEMBERS** - notify about new members and new canceled/unsubscribed members, set to `1` to enable
* **MIOUNNE_BMC_NOTIFY_EXTRAS** - notify about new extras purchases, set to `1` to enable
* **MIOUNNE_BMC_NOTIFY_SUPPORTERS** - notify about new one-time supporters, set to `1` to enable

### Matrix

Matrix bot

* **MIOUNNE_MATRIX_HOMESERVER** - homeserver url, eg: `https://matrix.example.com`
* **MIOUNNE_MATRIX_USER_LOGIN** - user localpart/login (eg: `miounne`)
* **MIOUNNE_MATRIX_USER_PASSWORD** - user password
* **MIOUNNE_MATRIX_USER_DISPLAYNAME** - (optional) display name of the matrix user, default: `Mother Miounne`

#### matrix-registration _(optional, set only if you want [matrix-registration](https://github.com/ZerataX/matrix-registration) integration)_

* **MIOUNNE_MATRIX_REGISTRATION_URL** - matrix-registration API URL/endpoint, note: you MUST set url with `/api` for newer versions of matrix-registration.
* **MIOUNNE_MATRIX_REGISTRATION_ROOM** - matrix-registration admin room ID, you should create that room yourself and invite Miounne
* **MIOUNNE_MATRIX_REGISTRATION_SECRET** - matrix-registration admin shared secret
* **MIOUNNE_MATRIX_REGISTRATION_NOTIFY_NEW** - send notifications about new invite tokens created
* **MIOUNNE_MATRIX_REGISTRATION_NOTIFY_USE** - send notifications after somebody uses an invite token

### Antispam _(optional)_

env vars:

* **MIOUNNE_SPAM_EMAILS** - list of spammers' email addresses, eg: `imaspammer@gmail.com honestlynotspammer@gmail.com`
* **MIOUNNE_SPAM_HOSTS** - list of spammers' hosts, eg: `google.com microsoft.com`

### Forms _(optional)_

env vars:

* **MIOUNNE_FORMS_NAME_URL** - Matrix webhook url of the `name` form (use instead of room ID)
* **MIOUNNE_FORMS_NAME_ROOM** - Matrix Room ID of the `name` form (use instead of webhook)
* **MIOUNNE_FORMS_NAME_IMG** - Matrix webhook avatar url of the `name` form
* **MIOUNNE_FORMS_NAME_FIELDS** - List of fields of the `name` form, eg: `name email notes`
* **MIOUNNE_FORMS_NAME_REDIRECT** - URL to redirect after handling form `name` data
* **MIOUNNE_FORMS_NAME_HONEYPOT** - honeypot field name of the `name` form
* **MIOUNNE_FORM_NAME_EXTENSIONS** - list of form extensions of the `name` form
* **MIOUNNE_FORM_NAME_RATELIMIT** - rate limit of the form, format: `<max requests>r/<interval:s,m>`, eg: `1r/s` or `54r/m`

1. Add form name to **MIOUNNE_FORMS**, eg: `export MIOUNNE_FORMS="form1 form2"`)
2. Add form config, eg: `export MIOUNNE_FORMS_FORM1_IMG=https://http.cat/200`

## Where to get

[Binary releases](https://gitlab.com/etke.cc/miounne/-/releases), [docker registry](https://gitlab.com/etke.cc/miounne/container_registry), [etke.cc](https://etke.cc)
