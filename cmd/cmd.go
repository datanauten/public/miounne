// Package main represends command-line tool
package main

import (
	"fmt"

	"git.sr.ht/~xn/bmc"
	"github.com/microcosm-cc/bluemonday"

	"gitlab.com/etke.cc/miounne/v2/configuration"
	"gitlab.com/etke.cc/miounne/v2/repositories/formext"
	"gitlab.com/etke.cc/miounne/v2/services"
	"gitlab.com/etke.cc/miounne/v2/utils"
)

var version string = "development"

func main() {
	var err error
	var bot services.MatrixBot
	config := configuration.New()

	if !config.Matrix.Enabled {
		panic("you must set matrix bot configuration")
	}

	log := utils.NewLogger("miounne.", config.Log.Level)
	log.Info("#############################")
	log.Info("Mother Miounne " + version)
	log.Info("HTTP: %t", config.Server.Port != "")
	log.Info("Forms: %d", len(config.Forms))
	log.Info("Matrix: %t", config.Matrix.Enabled)
	log.Info("-- buymeacoffee: %t", config.BMC.Enabled)
	log.Info("-- matrix-registration: %t", config.Matrix.Registration.Enabled)
	log.Info("#############################")

	policy := bluemonday.StrictPolicy()
	bmclient := createBMClient(&config.BMC, policy)
	bot, err = createMatrixBot(&config.Matrix, config.Frequency, config.Log.Level, bmclient)
	if err != nil {
		panic(err)
	}
	go bot.Start(config.Matrix.User.DisplayName)
	defer bot.Stop()

	rls := createRateLimiters(config.Forms, utils.NewLogger("ratelimit.", config.Log.Level))
	formHandler := createFormhandler(config.Forms, config.Spam, policy, bot, config.Log.Level)
	httpd := createHTTPServer(config.Server.Port, formHandler, rls, config.Log.Level, config.Log.IP)

	httpd.Start()
}

func createFormhandler(forms []*configuration.Form, spam configuration.Spam, policy services.Policy, sender services.FormSender, logLevel string) services.FormHandler {
	log := utils.NewLogger("forms.", logLevel)
	netvalid := utils.NewNetworkValidator(logLevel)
	exts := formext.NewExtensions(netvalid)
	return services.NewFormHandler(forms, exts, spam, netvalid, policy, sender, log)
}

func createHTTPServer(port string, formHandler services.FormHandler, rls map[string]services.RateLimiter, logLevel string, logIP bool) services.HTTPServer {
	ipMatcher := utils.NewIPMatcher(logIP)
	log := utils.NewLogger("http.", logLevel)

	return services.NewHTTPServer(port, log, ipMatcher, formHandler, rls)
}

func createMatrixBot(config *configuration.Matrix, frequency int, logLevel string, bmclient services.BMC) (services.MatrixBot, error) {
	var matrixRegistrationClient services.MatrixRegistrationClient
	client, syncer, err := services.NewMatrixClient(config.Homeserver, config.User.Login, config.User.Password)
	if err != nil {
		return nil, fmt.Errorf("cannot create matrix client: %v", err)
	}
	logger := utils.NewLogger("matrix.", logLevel)

	if config.Registration.Enabled {
		matrixRegistrationClient = services.NewMatrixRegistrationClient(
			config.Registration.URL,
			config.Registration.Secret,
			config.Registration.RoomID,
			config.Registration.Notify.New,
			config.Registration.Notify.Use,
		)
	}

	return services.NewMatrixBot(client, syncer, logger, frequency, matrixRegistrationClient, bmclient), nil
}

func createBMClient(config *configuration.BMC, policy services.Policy) services.BMC {
	if !config.Enabled {
		return nil
	}
	bmclient := bmc.NewClient(config.Token)

	return services.NewBMCClient(bmclient, config.RoomID, policy, config.Notify.Extras, config.Notify.Members, config.Notify.Supporters)
}

func createRateLimiters(forms []*configuration.Form, log services.Logger) map[string]services.RateLimiter {
	limiters := make(map[string]services.RateLimiter, len(forms))
	for _, form := range forms {
		if form.Ratelimit != "" {
			limiters[form.Name] = services.NewRateLimiter(form.Ratelimit, log)
		}
	}
	return limiters
}
