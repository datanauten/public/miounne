// Package formext represents form extensions
package formext

import (
	"sort"

	"gitlab.com/etke.cc/miounne/v2/utils"
)

// Extension is representation of form extension
type Extension interface {
	ExecuteMarkdown(name string, data map[string]string) string
	ExecuteHTML(name string, data map[string]string) string
}

// NewExtensions returns map of ext objects
func NewExtensions(netvalid utils.NetworkValidator) map[string]Extension {
	return map[string]Extension{
		"root":    NewRoot(),
		"etkecc":  NewEtkecc(netvalid),
		"example": NewExample(),
	}
}

// NewRoot parser/extension
func NewRoot() Extension {
	return &root{}
}

// NewExample parser/extension
func NewExample() Extension {
	return &example{}
}

// NewEtkecc parser/extension
func NewEtkecc(netvalid utils.NetworkValidator) Extension {
	return &etkecc{nv: netvalid}
}

// sortKeys from the form data
func sortKeys(data map[string]string) []string {
	keys := make([]string, 0, len(data))
	for key := range data {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	return keys
}
