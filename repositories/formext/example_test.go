package formext

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ExampleExtensionSuite struct {
	suite.Suite
}

func (suite *ExampleExtensionSuite) SetupTest() {
	suite.T().Helper()
}

func (suite *ExampleExtensionSuite) TearDownTest() {
	suite.T().Helper()
}

func (suite *ExampleExtensionSuite) TestExecuteMarkdown() {
	expected := "**Example**"

	actual := NewExample().ExecuteMarkdown(name, nil)

	suite.Equal(expected, actual)
}

func (suite *ExampleExtensionSuite) TestExecuteHTML() {
	expected := "<b>Example</b>"

	actual := NewExample().ExecuteHTML(name, nil)

	suite.Equal(expected, actual)
}

func TestExampleExtensionSuite(t *testing.T) {
	suite.Run(t, new(ExampleExtensionSuite))
}
