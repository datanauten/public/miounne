package formext

// example form extension/parser
type example struct{}

// ExecuteMarkdown example extension/parser
func (ext *example) ExecuteMarkdown(_ string, _ map[string]string) string {
	return "**Example**"
}

// ExecuteHTML example extension/parser
func (ext *example) ExecuteHTML(_ string, _ map[string]string) string {
	return "<b>Example</b>"
}
