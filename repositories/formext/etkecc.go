package formext

import (
	"crypto/rand"
	"math/big"
	"strings"

	"gitlab.com/etke.cc/miounne/v2/utils"
)

// etkecc extension is for specific etke.cc internal usecases. It's a prototype, done in spagetti-code way to simplify implementations of new ideas
type etkecc struct {
	nv   utils.NetworkValidator
	test bool
}

const (
	passwordLength  = 64
	passwordCharset = "abcdedfghijklmnopqrstABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" // a-z A-Z 0-9
)

var (
	passwordCharsetLength = big.NewInt(int64(len(passwordCharset)))
	preprocessFields      = []string{"email", "domain", "username"}
	defaults              = map[string]string{
		"domain":   "TODO.com",
		"username": "TODO",
		"email":    "email@TODO.com",
	}
)

// ExecuteMarkdown etkecc extension/parser
func (ext *etkecc) ExecuteMarkdown(name string, data map[string]string) string {
	data = ext.preprocess(data)
	out := ""

	out += "```yaml\n"
	dns := ext.generateDNS(data)
	questions := ext.generateQuestions(data)
	for _, question := range questions {
		out += question + "\n\n"
	}
	out += "DNS - please, add following entries:"
	out += dns
	out += "```\n\n"

	vars := ext.generateVars(data)
	out += "vars.yml:\n\n"
	out += "```yaml"
	out += vars
	out += "```\n\n"

	onboarding := ext.generateOnboarding(data)
	out += "onboarding:\n\n"
	out += "```md"
	out += onboarding
	out += "```\n\n"

	return out
}

// ExecuteHTML etkecc extension/parser
func (ext *etkecc) ExecuteHTML(name string, data map[string]string) string {
	return "HTML output is not implemented"
}

func (ext *etkecc) generatePassword() string {
	// ONLY for unit tests
	if ext.test {
		return "TODO"
	}
	var password strings.Builder
	for i := 0; i < passwordLength; i++ {
		// nolint // the configuration will be genered as template and must be modified manually after that, so even if password will not be generated that's not a problem
		index, _ := rand.Int(rand.Reader, passwordCharsetLength)
		password.WriteByte(passwordCharset[index.Int64()])
	}
	return password.String()
}

func (ext *etkecc) getVar(field string, data map[string]string) string {
	value := data[field]
	if value == "" {
		return defaults[field]
	}

	return value
}

// preprocess data
func (ext *etkecc) preprocess(data map[string]string) map[string]string {
	for _, key := range preprocessFields {
		data[key] = strings.ToLower(strings.TrimSpace(data[key]))
	}

	data["domain"] = utils.GetBaseDomain(data["domain"])
	data["serve_base_domain"] = "no"
	if ext.shouldServeBaseDomain(data["domain"]) {
		data["serve_base_domain"] = "yes"
	}

	hs, hsOK := data["homeserver"]
	if hs == "" || !hsOK {
		data["homeserver"] = "synapse"
	}

	return data
}

// shouldServeBaseDomain check for DNS entries on base domain
func (ext *etkecc) shouldServeBaseDomain(domain string) bool {
	if domain == "" {
		return true
	}

	return !ext.nv.HasA(domain) && !ext.nv.HasCNAME(domain)
}

func (ext *etkecc) generateQuestions(data map[string]string) []string {
	questions := []string{
		"Hi there,\nwe got your order and have some questions before the setup.",
	}
	questions = append(questions, ext.generateQuestionsSystem(data)...)
	questions = append(questions, ext.generateQuestionsBots(data)...)
	questions = append(questions, ext.generateQuestionsComponents(data)...)
	questions = append(questions, ext.generateQuestionsAdditionalServices(data)...)
	questions = append(questions, ext.generateQuestionsAdvancedComponents(data)...)

	if data["type"] == "turnkey" {
		questions = append(
			questions,
			"SSH: please, share with me your public ssh key and your public static IP(-s) to get ssh root access to your server",
		)
	}
	if data["type"] == "" || data["type"] == "byos" {
		questions = append(
			questions,
			"Server: please, create a VPS with any debian-based distro. Minimal comfortable configuration for a basic matrix server: 1vCPU, 2GB RAM.\nAdd my ssh key (https://etke.cc/ssh.key) to your server, share with me your server IP, username (with permissions to call sudo) and password (if set).",
		)
	}

	return questions
}

func (ext *etkecc) generateQuestionsSystem(data map[string]string) []string {
	domain := ext.getVar("domain", data)
	questions := []string{}
	if data["serve_base_domain"] != "yes" {
		question := "I see that you have something on your base domain, in that case you should add following HTTPS redirects (HTTP 301):\n"
		question += "* https://" + domain + "/.well-known/matrix/server -> https://matrix." + domain + "/.well-known/matrix/server\n"
		question += "* https://" + domain + "/.well-known/matrix/client -> https://matrix." + domain + "/.well-known/matrix/client"

		questions = append(questions, question)
	}

	return questions
}

func (ext *etkecc) generateQuestionsBots(data map[string]string) []string {
	questions := []string{}
	if data["reminder-bot"] != "" {
		questions = append(
			questions,
			"Reminder bot: What's your timezone (IANA)? Like America/Chicago, Asia/Seoul or Europe/Berlin",
		)
	}

	return questions
}

func (ext *etkecc) generateQuestionsComponents(data map[string]string) []string {
	questions := []string{}
	if data["telegram"] != "" {
		questions = append(
			questions,
			"Telegram: Please, go to https://my.telegram.org/apps and create a new app. Share the API ID and Hash with me",
		)
	}

	// smtp relay for own server
	if data["smtp-relay"] != "" && data["type"] != "turnkey" {
		questions = append(
			questions,
			"SMTP relay: please, select suitable email provider (big providers like Gmail or Outlook will ban you for automated mails, so you need to find a service that allows sending of verification emails. Optionally, we provide such service). Please, send me an SMTP host, SMTP STARTTLS port, SMTP login, SMTP password, SMTP email (usually login and email is the same thing, but that depends on provider).",
		)
	}

	// smtp relay for turnkey
	if data["smtp-relay"] != "" && data["type"] == "turnkey" {
		questions = append(
			questions,
			"Matrix email: please, create a matrix mailbox in your migadu admin dashboard and share with me mailbox email and password.",
		)
	}

	return questions
}

func (ext *etkecc) generateQuestionsAdditionalServices(data map[string]string) []string {
	questions := []string{}
	hasWgOrDnsmasq := data["wireguard"] != "" || data["dnsmasq"] != ""
	if data["miniflux"] != "" {
		questions = append(
			questions,
			"Miniflux: are you sure you want it? It's a RSS reader, not integrated to matrix. Please, check https://miniflux.app and decide.",
		)
	}

	if data["languagetool"] != "" {
		questions = append(
			questions,
			"Languagetool: are you sure you want it? It's an 'opensource grammarly' server, requires ~30GB of disk space for n-grams and not integrated to matrix. Please, check https://languagetool.org and decide.",
		)
	}

	if hasWgOrDnsmasq {
		questions = append(questions, ext.generateAdditionalServicesWireguard(data["wireguard"], data["dnsmasq"]))
	}

	if data["miounne"] != "" {
		questions = append(
			questions,
			"Miounne: are you sure you want it? Miounne is a bridge between external services (like html/http forms, matrix-registration, buymeacoffee, etc.) and matrix. Please, check https://gitlab.com/etke.cc/miounne and decide. If you still want it, please, send me a configuration to apply (no, there is no 'default configuration'. No, there is no 'good configuration'. No, we don't provide configuration templates. It's completely up to you)",
		)
	}

	return questions
}

func (ext *etkecc) generateAdditionalServicesWireguard(wireguard, dnsmasq string) string {
	wgWdnsmasq := wireguard != "" && dnsmasq != ""
	wgWOdnsmasq := wireguard != "" && dnsmasq == ""
	dnsmasqWOwg := dnsmasq != "" && wireguard == ""

	switch {
	case wgWdnsmasq:
		// wireguard with dnsmasq
		return "WireGuard: are you sure you want it? It's a VPN, not integrated to matrix. Please, check https://wireguard.com and decide. If you still want it, please, share with me a list of labels you want to assign to generated client keys (just to set filename, so even '1,2,3...') is OK"
	case wgWOdnsmasq:
		// wireguard without dnsmasq
		return "WireGuard: are you sure you want it (and without dnsmasq)? It's a VPN, not integrated to matrix. And dnsmasq is a caching recursive resolver, works best inside wireguard tunnels. Please, check https://wireguard.com and decide. If you still want it, please, share with me a list of labels you want to assign to generated client keys (just to set filename, so even '1,2,3...') is OK"
	case dnsmasqWOwg:
		// dnsmasq without wireguard
		return "dnsmasq: are you sure you want it without wireguard? Dnsmasq is a caching recursive resolver, works best in wireguard tunnels."
	default:
		return ""
	}
}

func (ext *etkecc) generateQuestionsAdvancedComponents(data map[string]string) []string {
	questions := []string{}
	if data["matrix-corporal"] != "" {
		questions = append(
			questions,
			"Corporal: are you sure you want it? Matrix Corporal is a 'kubernetes for matrix', based on policies. It may be suitable for large organizations intranets or large public servers, but it's too complex for private and small servers. I suggest you to skip it, but if you still want it - please, provide a policy that you want to set (no, there is no default policy. No, there is no 'some good policy'. No, we don't provide any policies, that's completely up to you).",
		)
	}

	if data["synapse-simple-antispam"] != "" {
		questions = append(
			questions,
			"Simple antispam: are you sure you want it? It's a synapse module to ban other matrix servers on your HS. It's not suitable for moderation. I suggest you to skip it (because you will have moderation tools directly in Element client and synapse-admin), if you still want it - please, provide a blocklist you want to include (no, there is no default ban list. No, there is no 'some good banlist'. No, we don't provide any banlists, that's completely up to you).",
		)
	}

	if data["synapse-workers"] != "" {
		questions = append(
			questions,
			"Synapse workers: are you sure you want them? Workers suitable only if your homeserver has 1000+ active real users and require additional compute power to work. I suggest you to skip them.",
		)
	}

	if data["go-neb"] != "" {
		questions = append(
			questions,
			"Go NEB: are you sure you want it? You have go neb by default in Element integration manager, hosted by New Vector. I suggest you to skip it, if you still want it - please, provide a configuration (ref: https://github.com/matrix-org/go-neb#configuring-services) you want to include (no, there is no default configuration. No there is no 'some good configuration'. No, we don't provide any custom configuration, that's completely up to you).",
		)
	}

	if data["mjolnir"] != "" {
		questions = append(
			questions,
			"Mjolnir: are you sure you want it? Mjolnir used as automated moderation system on large public homeservers, but for your usecase even standard moderation tools (included in Element client and synapse-admin by default) should be enough. I suggest you to skip it and think about it later when you will be sure that you can't run a server without it.",
		)
	}

	if data["stats"] != "" && data["type"] != "turnkey" {
		questions = append(
			questions,
			"Prometheus+Grafana: are you sure you want it? Any cloud provider gives you some dashboard with server stats, why not use that dashboard? Prometheus+Grafana stack provides some internal matrix stats (like count of events), but it's overkill if you just want to see server utilization. I suggest you to skip it.",
		)
	}

	return questions
}

// generateDNS instructions
// nolint // as-is
func (ext *etkecc) generateDNS(data map[string]string) string {
	dns := "\n"
	if data["serve_base_domain"] == "yes" {
		dns += strings.Join([]string{"@", "A record", "server ip\n"}, "\t")
	}
	dns += strings.Join([]string{"matrix", "A record", "server ip\n"}, "\t")
	if data["cinny"] != "" {
		dns += strings.Join([]string{"cinny", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["element-web"] != "" {
		dns += strings.Join([]string{"element", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["go-neb"] != "" {
		dns += strings.Join([]string{"goneb", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["hydrogen"] != "" {
		dns += strings.Join([]string{"hydrogen", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["jitsi"] != "" {
		dns += strings.Join([]string{"jitsi", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["kuma"] != "" {
		dns += strings.Join([]string{"kuma", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["languagetool"] != "" {
		dns += strings.Join([]string{"languagetool", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["miniflux"] != "" {
		dns += strings.Join([]string{"miniflux", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["miounne"] != "" {
		dns += strings.Join([]string{"miounne", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["stats"] != "" {
		dns += strings.Join([]string{"stats", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}
	if data["radicale"] != "" {
		dns += strings.Join([]string{"radicale", "CNAME record", "matrix." + ext.getVar("domain", data) + "\n"}, "\t")
	}

	return dns
}

// generateOnboarding instructions
func (ext *etkecc) generateOnboarding(data map[string]string) string {
	var out strings.Builder

	out.WriteString(ext.generateOnboardingLinks(data))
	out.WriteString(ext.generateOnboardingBots(data))
	out.WriteString(ext.generateOnboardingBridges(data))

	out.WriteString(ext.generateOnboardingCredentials(data))
	out.WriteString(ext.generateOnboardingPayment(data))

	return out.String()
}

// generateOnboardingLinks block
func (ext *etkecc) generateOnboardingLinks(data map[string]string) string {
	out := "\n# links\n\n"
	out += "* homeserver: https://matrix." + ext.getVar("domain", data) + "\n"
	out += "* synapse-admin: https://matrix." + ext.getVar("domain", data) + "/synapse-admin\n"
	if data["cinny"] != "" {
		out += "* cinny: https://cinny." + ext.getVar("domain", data) + "\n"
	}
	if data["element-web"] != "" {
		out += "* element: https://element." + ext.getVar("domain", data) + "\n"
	}
	if data["hydrogen"] != "" {
		out += "* hydrogen: https://hydrogen." + ext.getVar("domain", data) + "\n"
	}
	if data["jitsi"] != "" {
		out += "* jitsi: https://jitsi." + ext.getVar("domain", data) + "\n"
	}
	if data["kuma"] != "" {
		out += "* uptime-kuma: https://kuma." + ext.getVar("domain", data) + "\n"
	}
	if data["languagetool"] != "" {
		out += "* languagetool: https://languagetool." + ext.getVar("domain", data) + "\n"
	}
	if data["miniflux"] != "" {
		out += "* miniflux: https://miniflux." + ext.getVar("domain", data) + "\n"
	}
	if data["miounne"] != "" {
		out += "* miounne: https://miounne." + ext.getVar("domain", data) + "\n"
	}
	if data["stats"] != "" {
		out += "* stats: https://stats." + ext.getVar("domain", data) + "\n"
	}
	if data["radicale"] != "" {
		out += "* radicale: https://radicale." + ext.getVar("domain", data) + "\n"
	}

	return out
}

// generateOnboardingBots block
func (ext *etkecc) generateOnboardingBots(data map[string]string) string {
	out := ""
	if data["go-neb"] != "" {
		out += "* go-neb: @goneb:" + ext.getVar("domain", data) + "\n"
	}
	if data["miounne"] != "" {
		out += "* miounne: @miounne:" + ext.getVar("domain", data) + "\n"
	}
	if data["mjolnir"] != "" {
		out += "* mjolnir: @mjolnir:" + ext.getVar("domain", data) + "\n"
	}
	if data["reminder-bot"] != "" {
		out += "* reminder: @reminder:" + ext.getVar("domain", data) + "\n"
	}

	if out != "" {
		out = "\n# bots\n\n" + out
	}

	return out
}

// generateOnboardingBridges block
// nolint // just as-is
func (ext *etkecc) generateOnboardingBridges(data map[string]string) string {
	domain := ext.getVar("domain", data)
	out := ""
	if data["discord"] != "" {
		out += "* discord: @_discordpuppet_bot:" + domain + "\n"
	}
	if data["facebook"] != "" {
		out += "* facebook: @facebookbot:" + domain + "\n"
	}
	if data["googlechat"] != "" {
		out += "* google chat: @googlechatbot:" + domain + "\n"
	}
	if data["groupme"] != "" {
		out += "* groupme: @_groupmepuppet_bot:" + domain + "\n"
	}
	if data["instagram"] != "" {
		out += "* instagram: @instagrambot:" + domain + "\n"
	}
	if data["irc"] != "" {
		out += "* irc: @heisenbridge:" + domain + "\n"
	}
	if data["linkedin"] != "" {
		out += "* linkedin: @linkedinbot:" + domain + "\n"
	}
	if data["signal"] != "" {
		out += "* signal: @signalbot:" + domain + "\n"
	}
	if data["skype"] != "" {
		out += "* skype: @_skypepuppet_bot:" + domain + "\n"
	}
	if data["slack"] != "" {
		out += "* slack: @_slackpuppet_bot:" + domain + "\n"
	}
	if data["steam"] != "" {
		out += "* steam: @_steampuppet_bot:" + domain + "\n"
	}
	if data["telegram"] != "" {
		out += "* telegram: @telegrambot:" + domain + "\n"
	}
	if data["twitter"] != "" {
		out += "* twitter: @twitterbot:" + domain + "\n"
	}
	if data["whatsapp"] != "" {
		out += "* whatsapp: @whatsappbot:" + domain + "\n"
	}

	if out != "" {
		out = "\n# bridges\n\n" + out + "\n > You can find instructions for any bridge here: https://etke.cc/bridges\n"
	}

	return out
}

// generateOnboardingCredentials block
func (ext *etkecc) generateOnboardingCredentials(data map[string]string) string {
	out := "\n# credentials\n\n"
	out += "* login: " + ext.getVar("username", data) + "\n"
	out += "* password: " + ext.generatePassword() + "\n"
	if data["radicale"] != "" {
		out += "* radicale password: TODO\n"
	}
	out += "* mxid: @" + ext.getVar("username", data) + ":" + ext.getVar("domain", data) + "\n\n"
	out += "In case of any issues: @support:etke.cc\n\n"
	out += "> **NOTE**: don't forget to change the password as soon as you login for the first time!\n"

	return out
}

// generateOnboardingPayment block
func (ext *etkecc) generateOnboardingPayment(data map[string]string) string {
	if ext.getVar("type", data) == "turnkey" {
		return ""
	}

	return `
# payment

Please, [buy the Setup item](https://etke.cc/setup).

If you want an ongoing maintenance of your server (host/system maintenance, matrix components maintenance, updates and reconfiguration),
join the **Maintenance** membership on [https://etke.cc/membership](https://etke.cc/membership).
Please, clarify now if you want to join the maintenance membership, because if not - we will remove any your configuration and credentials from our side.

> **NOTE**: all prices are based on [Pay What You Want](https://en.wikipedia.org/wiki/Pay_what_you_want) model with minimal (floor) price set
`
}

// generateVars yml
func (ext *etkecc) generateVars(data map[string]string) string {
	var out strings.Builder

	// core components
	out.WriteString(ext.generateVarsSystem())
	out.WriteString(ext.generateVarsHomeserver(data))
	out.WriteString(ext.generateVarsSynapse(data))
	out.WriteString(ext.generateVarsDendrite(data))
	out.WriteString(ext.generateVarsPostgresBackups())
	out.WriteString(ext.generateVarsCorporal(data))
	out.WriteString(ext.generateVarsProxy(data))

	// matrix-related services
	out.WriteString(ext.generateVarsCinny(data))
	out.WriteString(ext.generateVarsElement(data))
	out.WriteString(ext.generateVarsJitsi(data))
	out.WriteString(ext.generateVarsStats(data))
	out.WriteString(ext.generateVarsSynapseAdmin(data))

	// services
	out.WriteString(ext.generateVarsDnsmasq(data))
	out.WriteString(ext.generateVarsKuma(data))
	out.WriteString(ext.generateVarsLanguagetool(data))
	out.WriteString(ext.generateVarsMiniflux(data))
	out.WriteString(ext.generateVarsMiounne(data))
	out.WriteString(ext.generateVarsRadicale(data))
	out.WriteString(ext.generateVarsWireguard(data))

	// bots
	out.WriteString(ext.generateVarsGoneb(data))
	out.WriteString(ext.generateVarsMjolnir(data))
	out.WriteString(ext.generateVarsReminder(data))

	// bridges
	out.WriteString(ext.generateVarsDiscord(data))
	out.WriteString(ext.generateVarsFacebook(data))
	out.WriteString(ext.generateVarsGooglechat(data))
	out.WriteString(ext.generateVarsGroupme(data))
	out.WriteString(ext.generateVarsIRC(data))
	out.WriteString(ext.generateVarsInstagram(data))
	out.WriteString(ext.generateVarsLinkedin(data))
	out.WriteString(ext.generateVarsSignal(data))
	out.WriteString(ext.generateVarsSkype(data))
	out.WriteString(ext.generateVarsSlack(data))
	out.WriteString(ext.generateVarsSteam(data))
	out.WriteString(ext.generateVarsTelegram(data))
	out.WriteString(ext.generateVarsTwitter(data))
	out.WriteString(ext.generateVarsWebhooks(data))
	out.WriteString(ext.generateVarsWhatsapp(data))

	return out.String()
}

// generateVarsSystem block
func (ext *etkecc) generateVarsSystem() string {
	out := "\n# system\n"
	out += "system_security_autorizedkeys: []\n"

	return out
}

// generateVarsHomeserver block
func (ext *etkecc) generateVarsHomeserver(data map[string]string) string {
	out := "\n# homeserver https://matrix." + ext.getVar("domain", data) + "\n"
	out += "matrix_domain: " + ext.getVar("domain", data) + "\n"
	out += "matrix_ssl_lets_encrypt_support_email: " + ext.getVar("email", data) + "\n"
	out += "matrix_ma1sd_enabled: no\n"
	out += "matrix_mailer_enabled: no\n"

	return out
}

// generateVarsPostgresBackups block
func (ext *etkecc) generateVarsPostgresBackups() string {
	out := "\n# postgres::backups\n"
	out += "matrix_postgres_backup_enabled: yes\n"
	out += "matrix_postgres_backup_schedule: '@daily'\n"
	out += "matrix_postgres_backup_keep_days: 7\n"
	out += "matrix_postgres_backup_keep_weeks: 0\n"
	out += "matrix_postgres_backup_keep_months: 0\n"

	return out
}

// generateVarsSynapse blocks
func (ext *etkecc) generateVarsSynapse(data map[string]string) string {
	out := "\n# synapse\n"
	out += "matrix_homeserver_implementation: synapse\n"
	out += "matrix_synapse_presence_enabled: yes\n"
	out += "matrix_synapse_enable_group_creation: yes\n"
	out += "matrix_synapse_max_upload_size_mb: 1024\n"
	out += "matrix_synapse_tmp_directory_size_mb: \"{{ matrix_synapse_max_upload_size_mb * 2 }}\"\n"

	out += "\n# synapse::federation\n"
	out += "matrix_synapse_allow_public_rooms_over_federation: yes\n"
	out += "matrix_synapse_allow_public_rooms_without_auth: yes\n"

	if data["smtp-relay"] != "" {
		out += "\n# synapse::mailer\n"
		out += "matrix_synapse_email_enabled: yes\n"
		out += "matrix_synapse_email_smtp_host: TODO\n"
		out += "matrix_synapse_email_smtp_port: 587\n"
		out += "matrix_synapse_email_smtp_user: TODO\n"
		out += "matrix_synapse_email_smtp_pass: TODO\n"
		out += "matrix_synapse_email_notif_from: \"Matrix <matrix@{{ matrix_domain }}>\"\n"
		out += "matrix_synapse_email_client_base_url: \"https://{{ matrix_server_fqn_element }}\"\n"
		out += "matrix_synapse_email_invite_client_location: \"https://{{ matrix_server_fqn_element }}\"\n"
	}

	out += "\n# synapse::credentials\n"
	out += "matrix_synapse_macaroon_secret_key: " + ext.generatePassword() + "\n"
	out += "matrix_postgres_connection_password: " + ext.generatePassword() + "\n"
	out += "matrix_synapse_password_config_pepper: " + ext.generatePassword() + "\n"
	out += "matrix_coturn_turn_static_auth_secret: " + ext.generatePassword() + "\n"
	out += "matrix_homeserver_generic_secret_key: \"{{ matrix_synapse_macaroon_secret_key }}\"\n"

	out += "\n# synapse::custom\n"
	out += "matrix_synapse_configuration_extension_yaml: |\n"
	out += "  disable_msisdn_registration: yes\n"
	out += "  allow_device_name_lookup_over_federation: no\n"

	out += "\n# synapse::privacy\n"
	out += "matrix_synapse_user_ips_max_age: 5m\n"
	out += "matrix_synapse_redaction_retention_period: 5m\n"

	out += "\n# synapse::extensions::shared_secret_auth\n"
	out += "matrix_synapse_ext_password_provider_shared_secret_auth_enabled: yes\n"
	out += "matrix_synapse_ext_password_provider_shared_secret_auth_shared_secret: " + ext.generatePassword() + "\n"

	if data["synapse-simple-antispam"] != "" {
		out += "\n# synapse::extensions::simple-antispam\n"
		out += "matrix_synapse_ext_spam_checker_synapse_simple_antispam_enabled: yes\n"
		out += "matrix_synapse_ext_spam_checker_synapse_simple_antispam_config_blocked_homeservers: []\n"
	}

	if data["mjolnir"] != "" {
		out += "\n# synapse::extensions::spam_checker_mjolnir\n"
		out += "matrix_synapse_ext_spam_checker_mjolnir_antispam_enabled: yes\n"
		out += "matrix_synapse_ext_spam_checker_mjolnir_antispam_config_block_invites: no\n"
		out += "matrix_synapse_ext_spam_checker_mjolnir_antispam_config_block_messages: no\n"
		out += "matrix_synapse_ext_spam_checker_mjolnir_antispam_config_block_usernames: no\n"
		out += "matrix_synapse_ext_spam_checker_mjolnir_antispam_config_ban_lists: []\n"
	}

	if data["matrix-corporal"] != "" {
		out += "\n# synapse::extensions::rest_auth\n"
		out += "matrix_synapse_ext_password_provider_rest_auth_enabled: yes\n"
		out += "matrix_synapse_ext_password_provider_rest_auth_endpoint: \"http://matrix-corporal:41080/_matrix/corporal\"\n"
	}

	if data["synapse-workers"] != "" {
		out += "\n# synapse::workers\n"
		out += "matrix_synapse_workers_enabled: yes\n"
		out += "matrix_synapse_workers_preset: one-of-each\n"
	}

	return out
}

// TODO
func (ext *etkecc) generateVarsDendrite(data map[string]string) string {
	if data["homeserver"] != "dendrite" {
		return ""
	}

	out := "\n# dendrite\n"
	out += "matrix_homeserver_implementation: dendrite\n"

	return out
}

// generateVarsCorporal block
func (ext *etkecc) generateVarsCorporal(data map[string]string) string {
	out := ""
	if data["matrix-corporal"] != "" {
		out += "\n# matrix corporal\n"
		out += "matrix_corporal_enabled: yes\n"
		out += "matrix_corporal_http_api_enabled: yes\n"
		out += "matrix_corporal_http_api_auth_token: " + ext.generatePassword() + "\n"
		out += "matrix_corporal_corporal_user_id_local_part: \"corporal\" # password: " + ext.generatePassword() + "\n"
		out += "matrix_corporal_policy_provider_config: |\n"
		out += "  # TODO\n"
	}

	return out
}

// generateVarsSynapseAdmin block
func (ext *etkecc) generateVarsSynapseAdmin(data map[string]string) string {
	out := "\n# synapse-admin https://matrix." + ext.getVar("domain", data) + "/synapse-admin\n"
	out += "matrix_synapse_admin_enabled: yes\n"

	return out
}

// generateVarsProxy block
// nolint // as-is
func (ext *etkecc) generateVarsProxy(data map[string]string) string {
	out := "\n# nginx proxy\n"
	out += "matrix_nginx_proxy_access_log_enabled: no\n"
	out += "matrix_nginx_proxy_base_domain_serving_enabled: " + data["serve_base_domain"] + "\n"
	out += "matrix_nginx_proxy_base_domain_homepage_enabled: no\n"

	hasDomains := data["languagetool"] != "" || data["miniflux"] != "" || data["miounne"] != "" || data["kuma"] != "" || data["radicale"] != ""

	if !hasDomains {
		return out
	}

	out += "matrix_ssl_additional_domains_to_obtain_certificates_for:\n"
	if data["kuma"] != "" {
		out += "- \"{{ matrix_server_fqn_kuma }}\"\n"
	}
	if data["languagetool"] != "" {
		out += "- \"{{ matrix_server_fqn_languagetool }}\"\n"
	}
	if data["miniflux"] != "" {
		out += "- \"{{ matrix_server_fqn_miniflux }}\"\n"
	}
	if data["miounne"] != "" {
		out += "- \"{{ matrix_server_fqn_miounne }}\"\n"
	}
	if data["radicale"] != "" {
		out += "- \"{{ matrix_server_fqn_radicale }}\"\n"
	}

	return out
}

// generateVarsCinny block
func (ext *etkecc) generateVarsCinny(data map[string]string) string {
	out := ""
	if data["cinny"] != "" {
		out += "\n# cinny https://cinny." + ext.getVar("domain", data) + "\n"
		out += "matrix_client_cinny_enabled: yes\n"
	}

	return out
}

// generateVarsElement block
func (ext *etkecc) generateVarsElement(data map[string]string) string {
	out := ""
	if data["element-web"] != "" {
		out += "\n# element https://element." + ext.getVar("domain", data) + "\n"
		out += "matrix_client_element_enabled: yes\n"
		out += "matrix_client_element_default_theme: dark\n"
		out += "matrix_server_fqn_element: \"element.{{ matrix_domain }}\"\n"
	}

	return out
}

// generateVarsJitsi block
func (ext *etkecc) generateVarsJitsi(data map[string]string) string {
	out := ""
	if data["jitsi"] != "" {
		out += "\n# jitsi https://jitsi." + ext.getVar("domain", data) + "\n"
		out += "matrix_jitsi_enabled: yes\n"
		out += "matrix_jitsi_jvb_auth_password: " + ext.generatePassword() + "\n"
		out += "matrix_jitsi_jibri_xmpp_password: " + ext.generatePassword() + "\n"
		out += "matrix_jitsi_jibri_recorder_password: " + ext.generatePassword() + "\n"
		out += "matrix_jitsi_jicofo_auth_password: " + ext.generatePassword() + "\n"
		out += "matrix_jitsi_jicofo_component_secret: " + ext.generatePassword() + "\n"
	}

	return out
}

// generateVarsStats block
func (ext *etkecc) generateVarsStats(data map[string]string) string {
	out := ""
	if data["stats"] != "" {
		out += "\n# stats https://stats." + ext.getVar("domain", data) + "\n"
		out += "matrix_grafana_enabled: yes\n"
		out += "matrix_prometheus_enabled: yes\n"
		out += "matrix_grafana_anonymous_access: no\n"
		out += "matrix_prometheus_node_exporter_enabled: yes\n"
		out += "matrix_grafana_default_admin_user: " + ext.getVar("username", data) + "\n"
		out += "matrix_grafana_default_admin_password: " + ext.generatePassword() + "\n"
	}

	return out
}

// generateVarsDnsmasq block
func (ext *etkecc) generateVarsDnsmasq(data map[string]string) string {
	out := ""
	if data["dnsmasq"] != "" {
		out += "\n# dnsmasq\n"
		out += "custom_dnsmasq_enabled: yes\n"
	}

	return out
}

// generateVarsKuma block
func (ext *etkecc) generateVarsKuma(data map[string]string) string {
	out := ""
	if data["kuma"] != "" {
		out += "\n# uptime-kuma https://kuma." + ext.getVar("domain", data) + "\n"
		out += "custom_kuma_enabled: yes\n"
		out += "matrix_server_fqn_kuma: \"kuma.{{ matrix_domain }}\"\n"
	}

	return out
}

// generateVarsLanguagetool block
func (ext *etkecc) generateVarsLanguagetool(data map[string]string) string {
	out := ""
	if data["languagetool"] != "" {
		out += "\n# languagetool https://languagetool." + ext.getVar("domain", data) + "\n"
		out += "custom_languagetool_enabled: yes\n"
		out += "custom_languagetool_ngrams_enabled: yes # WARNING: requires a LOT of storage\n"
		out += "matrix_server_fqn_languagetool: \"languagetool.{{ matrix_domain }}\"\n"
	}

	return out
}

// generateVarsRadicale block
func (ext *etkecc) generateVarsRadicale(data map[string]string) string {
	out := ""
	if data["radicale"] != "" {
		out += "\n# radicale https://radicale." + ext.getVar("domain", data) + "\n"
		out += "custom_radicale_enabled: yes\n"
		out += "matrix_server_fqn_radicale: \"radicale.{{ matrix_domain }}\"\n"
		out += "custom_radicale_htpasswd: \"" + ext.getVar("username", data) + ":TODO\"\n"
		out += "# TODO:\n"
		out += "# 1. htpasswd -nb " + ext.getVar("username", data) + " " + ext.generatePassword() + "\n"
		out += "# 2. add the password to onboarding list"
	}

	return out
}

// generateVarsMiniflux block
func (ext *etkecc) generateVarsMiniflux(data map[string]string) string {
	out := ""
	if data["miniflux"] != "" {
		password := ext.generatePassword()
		out += "\n# miniflux https://miniflux." + ext.getVar("domain", data) + "\n"
		out += "custom_miniflux_enabled: yes\n"
		out += "custom_miniflux_database_password: " + password + "\n"
		out += "matrix_server_fqn_miniflux: \"miniflux.{{ matrix_domain }}\"\n"
		out += "# TODO:\n"
		out += "# 1. SQL:\n"
		out += "# CREATE USER miniflux WITH PASSWORD '" + password + "';\n"
		out += "# CREATE DATABASE miniflux; GRANT ALL PRIVILEGES ON DATABASE miniflux to miniflux;\n"
		out += "# 2. Create user:\n"
		out += "# docker exec -it custom-miniflux /usr/bin/miniflux -create-admin\n"
	}

	return out
}

// generateVarsMiounne block. WARNING: recursion
func (ext *etkecc) generateVarsMiounne(data map[string]string) string {
	out := ""
	if data["miounne"] != "" {
		password := ext.generatePassword()
		out += "\n# miounne https://miounne." + ext.getVar("domain", data) + "\n"
		out += "custom_miounne_enabled: yes\n"
		out += "custom_miounne_matrix_user_login: miounne\n"
		out += "custom_miounne_matrix_user_password: " + password + "\n"

		out += "# TODO: only for registration\n"
		out += "custom_miounne_registration_url: https://matrix." + ext.getVar("domain", data) + "/matrix-registration\n"
		out += "custom_miounne_matrix_registration_room: TODO\n"
		out += "custom_miounne_matrix_registration_secret: TODO\n"

		out += "# TODO: only for BMC\n"
		out += "custom_miounne_bmc_token: TODO\n"
		out += "custom_miounne_bmc_room: TODO\n"
		out += "custom_miounne_bmc_notify_extras: 1\n"
		out += "custom_miounne_bmc_notify_members: 1\n"
		out += "custom_miounne_bmc_notify_supporters: 1\n"

		out += "# TODO: only for forms\n"
		out += "matrix_server_fqn_miounne: \"miounne.{{ matrix_domain }}\"\n"
		out += "matrix_nginx_proxy_proxy_miounne_hostname: \"{{ matrix_server_fqn_miounne }}\"\n"
		out += "custom_miounne_spam_emails: []\n"
		out += "custom_miounne_spam_hosts: []\n"
		out += "custom_miounne_forms: []\n"

		out += "# TODO: matrix-synapse-register-user miounne " + password + " 0\n"
	}

	return out
}

// generateVarsWireguard block
func (ext *etkecc) generateVarsWireguard(data map[string]string) string {
	out := ""
	if data["wireguard"] != "" {
		out += "\n# wireguard\n"
		out += "custom_wireguard_enabled: yes\n"
		out += "custom_wireguard_overwrite: no\n"
		out += "custom_wireguard_clients: [] # TODO\n"
	}

	return out
}

// generateVarsGoneb block
func (ext *etkecc) generateVarsGoneb(data map[string]string) string {
	out := ""
	if data["go-neb"] != "" {
		out += "\n# bots::goneb\n"
		out += "matrix_bot_go_neb_enabled: yes\n"
		out += "matrix_bot_go_neb_clients:\n"
		out += "- UserID: \"@goneb:{{ matrix_domain }}\"\n"
		out += "  AccessToken: \"TODO\" # password: " + ext.generatePassword() + "\n"
		out += "  DeviceID: server\n"
		out += "  HomeserverURL: \"{{ matrix_homeserver_container_url }}\"\n"
		out += "  Sync: yes\n"
		out += "  AutoJoinRooms: yes\n"
		out += "  DisplayName: \"GoNEB\"\n"
		out += "  AcceptVerificationFromUsers: [\":{{ matrix_domain }}\"]\n"
		out += "matrix_bot_go_neb_services: [] # TODO\n"
	}

	return out
}

// generateVarsMjolnir block
func (ext *etkecc) generateVarsMjolnir(data map[string]string) string {
	out := ""
	if data["mjolnir"] != "" {
		out += "\n# bots::mjolnir\n"
		out += "matrix_bot_mjolnir_enabled: yes\n"
		out += "matrix_bot_mjolnir_access_token: \"TODO\" # password: " + ext.generatePassword() + "\n"
		out += "matrix_bot_mjolnir_management_room: TODO\n"
		out += "matrix_bot_mjolnir_configuration_extension_yaml: |\n"
		out += "  recordIgnoredInvites: true\n"
	}

	return out
}

// generateVarsReminder block
func (ext *etkecc) generateVarsReminder(data map[string]string) string {
	out := ""
	if data["reminder-bot"] != "" {
		password := ext.generatePassword()
		out += "\n# bots::reminder\n"
		out += "matrix_bot_matrix_reminder_bot_enabled: yes\n"
		out += "matrix_bot_matrix_reminder_bot_reminders_timezone: TODO\n"
		out += "matrix_bot_matrix_reminder_bot_matrix_user_id_localpart: reminder\n"
		out += "matrix_bot_matrix_reminder_bot_matrix_user_password: " + password + "\n"

		out += "# TODO: matrix-synapse-register-user reminder " + password + " 0\n"
	}

	return out
}

// generateVarsDiscord block
func (ext *etkecc) generateVarsDiscord(data map[string]string) string {
	out := ""
	if data["discord"] != "" {
		out += "\n# bridges::discord\n"
		out += "matrix_mx_puppet_discord_enabled: yes\n"
		out += "matrix_mx_puppet_discord_configuration_extension_yaml: |\n"
		out += "  presence:\n"
		out += "    enabled: no\n"
		out += "  logging:\n"
		out += "    console: warn\n"
	}

	return out
}

// generateVarsFacebook block
func (ext *etkecc) generateVarsFacebook(data map[string]string) string {
	out := ""
	if data["facebook"] != "" {
		out += "\n# bridges::facebook\n"
		out += "matrix_mautrix_facebook_enabled: yes\n"
		out += "matrix_mautrix_facebook_bridge_presence: no\n"
		out += "matrix_mautrix_facebook_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_facebook_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      paho:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsGooglechat block
func (ext *etkecc) generateVarsGooglechat(data map[string]string) string {
	out := ""
	if data["googlechat"] != "" {
		out += "\n# bridges::googlechat\n"
		out += "matrix_mautrix_googlechat_enabled: yes\n"
		out += "matrix_mautrix_googlechat_bridge_presence: no\n"
		out += "matrix_mautrix_googlechat_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_googlechat_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      hangups:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsGroupme block
func (ext *etkecc) generateVarsGroupme(data map[string]string) string {
	out := ""
	if data["groupme"] != "" {
		out += "\n# bridges::groupme\n"
		out += "matrix_mx_puppet_groupme_enabled: yes\n"
		out += "matrix_mx_puppet_groupme_configuration_extension_yaml: |\n"
		out += "  presence:\n"
		out += "    enabled: no\n"
		out += "  logging:\n"
		out += "    console: warn\n"
	}

	return out
}

// generateVarsIRC block
func (ext *etkecc) generateVarsIRC(data map[string]string) string {
	out := ""
	if data["irc"] != "" {
		out += "\n# bridges::irc (heisenbridge)\n"
		out += "matrix_heisenbridge_enabled: yes\n"
		out += "matrix_heisenbridge_identd_enabled: yes\n"
		out += "matrix_heisenbridge_owner: \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\"\n"
	}

	return out
}

// generateVarsInstagram block
func (ext *etkecc) generateVarsInstagram(data map[string]string) string {
	out := ""
	if data["instagram"] != "" {
		out += "\n# bridges::instagram\n"
		out += "matrix_mautrix_instagram_enabled: yes\n"
		out += "matrix_mautrix_instagram_bridge_presence: no\n"
		out += "matrix_mautrix_instagram_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_instagram_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      mauigpapi:\n"
		out += "        level: WARNING\n"
		out += "      paho:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsLinkedin block
func (ext *etkecc) generateVarsLinkedin(data map[string]string) string {
	out := ""
	if data["linkedin"] != "" {
		out += "\n# bridges::linkedin\n"
		out += "matrix_beeper_linkedin_enabled: yes\n"
		out += "matrix_beeper_linkedin_bridge_presence: no\n"
		out += "matrix_beeper_linkedin_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_beeper_linkedin_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      paho:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsSignal block
func (ext *etkecc) generateVarsSignal(data map[string]string) string {
	out := ""
	if data["signal"] != "" {
		out += "\n# bridges::signal\n"
		out += "matrix_mautrix_signal_enabled: yes\n"
		out += "matrix_mautrix_signal_bridge_presence: no\n"
		out += "matrix_mautrix_signal_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_signal_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsSkype block
func (ext *etkecc) generateVarsSkype(data map[string]string) string {
	out := ""
	if data["skype"] != "" {
		out += "\n# bridges::skype\n"
		out += "matrix_mx_puppet_skype_enabled: yes\n"
		out += "matrix_mx_puppet_skype_configuration_extension_yaml: |\n"
		out += "  presence:\n"
		out += "    enabled: no\n"
		out += "  logging:\n"
		out += "    console: warn\n"
	}

	return out
}

// generateVarsSlack block
func (ext *etkecc) generateVarsSlack(data map[string]string) string {
	out := ""
	if data["slack"] != "" {
		out += "\n# bridges::slack\n"
		out += "matrix_mx_puppet_slack_enabled: yes\n"
		out += "matrix_mx_puppet_slack_configuration_extension_yaml: |\n"
		out += "  presence:\n"
		out += "    enabled: no\n"
		out += "  logging:\n"
		out += "    console: warn\n"
	}

	return out
}

// generateVarsSteam block
func (ext *etkecc) generateVarsSteam(data map[string]string) string {
	out := ""
	if data["steam"] != "" {
		out += "\n# bridges::steam\n"
		out += "matrix_mx_puppet_steam_enabled: yes\n"
		out += "matrix_mx_puppet_steam_configuration_extension_yaml: |\n"
		out += "  presence:\n"
		out += "    enabled: no\n"
		out += "  logging:\n"
		out += "    console: warn\n"
	}

	return out
}

// generateVarsTelegram block
func (ext *etkecc) generateVarsTelegram(data map[string]string) string {
	out := ""
	if data["telegram"] != "" {
		out += "\n# bridges::telegram\n"
		out += "matrix_mautrix_telegram_enabled: yes\n"
		out += "matrix_mautrix_telegram_api_id: TODO\n"
		out += "matrix_mautrix_telegram_api_hash: TODO\n"
		out += "matrix_mautrix_telegram_bridge_presence: no\n"
		out += "matrix_mautrix_telegram_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    delivery_error_reports: yes\n"
		out += "    delivery_receipts: no\n"
		out += "    max_initial_member_sync: 10\n"
		out += "    private_chat_portal_meta: yes\n"
		out += "    sync_channel_members: no\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_telegram_homeserver_domain }}\": full\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      telethon:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsTwitter block
func (ext *etkecc) generateVarsTwitter(data map[string]string) string {
	out := ""
	if data["twitter"] != "" {
		out += "\n# bridges::twitter\n"
		out += "matrix_mautrix_twitter_enabled: yes\n"
		out += "matrix_mautrix_twitter_bridge_presence: no\n"
		out += "matrix_mautrix_twitter_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    delivery_error_reports: yes\n"
		out += "    delivery_receipts: no\n"
		out += "    private_chat_portal_meta: yes\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_twitter_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    loggers:\n"
		out += "      mau:\n"
		out += "        level: WARNING\n"
		out += "      aiohttp:\n"
		out += "        level: WARNING\n"
		out += "    root:\n"
		out += "      level: WARNING\n"
		out += "      handlers: [console]\n"
	}

	return out
}

// generateVarsWebhooks block
func (ext *etkecc) generateVarsWebhooks(data map[string]string) string {
	out := ""
	if data["webhooks"] != "" {
		out += "\n# bridges::webhooks\n"
		out += "matrix_appservice_webhooks_enabled: yes\n"
		out += "matrix_appservice_webhooks_api_secret: " + ext.generatePassword() + "\n"
	}

	return out
}

// generateVarsWhatsapp block
func (ext *etkecc) generateVarsWhatsapp(data map[string]string) string {
	out := ""
	if data["whatsapp"] != "" {
		out += "\n# bridges::whatsapp\n"
		out += "matrix_mautrix_whatsapp_enabled: yes\n"
		out += "matrix_mautrix_whatsapp_bridge_presence: no\n"
		out += "matrix_mautrix_whatsapp_configuration_extension_yaml: |\n"
		out += "  bridge:\n"
		out += "    permissions:\n"
		out += "      \"{{ matrix_mautrix_whatsapp_homeserver_domain }}\": user\n"
		out += "      \"@" + ext.getVar("username", data) + ":{{ matrix_domain }}\": admin\n"
		out += "  logging:\n"
		out += "    print_level: warn\n"
	}

	return out
}
