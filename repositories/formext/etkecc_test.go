package formext

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/etke.cc/miounne/v2/mocks"
)

type EtkeccExtensionSuite struct {
	suite.Suite
	nv         *mocks.NetworkValidator
	ext        *etkecc
	orderFull  map[string]string
	orderEmpty map[string]string
}

func (suite *EtkeccExtensionSuite) SetupTest() {
	suite.T().Helper()

	suite.orderEmpty = map[string]string{
		"email":      "tEsT@TEST.cOm",
		"name":       "Test",
		"smtp-relay": "on",
		"stats":      "on",
		"wireguard":  "on",
		"homeserver": "dendrite",
		"type":       "byos",
		"notes":      "I don't want anything, submitted the form because the Send Request button is sooo cool",
	}

	suite.orderFull = map[string]string{
		"homeserver":              "synapse",
		"domain":                  "https://matrix.ExAmPlE.com ", // to test how base domain will be extracted
		"username":                " tEsT ",
		"email":                   "tEsT@TEST.cOm",
		"name":                    "Test",
		"notes":                   "ALL, I.WANT.THEM.ALLLLLLL. Yes, on shitty 1vCPU 1GB RAM VPS.",
		"type":                    "turnkey",
		"cinny":                   "on",
		"discord":                 "on",
		"dnsmasq":                 "on",
		"element-web":             "on",
		"facebook":                "on",
		"go-neb":                  "on",
		"googlechat":              "on",
		"groupme":                 "on",
		"hangouts":                "on",
		"hydrogen":                "on",
		"instagram":               "on",
		"irc":                     "on",
		"jitsi":                   "on",
		"kuma":                    "on",
		"languagetool":            "on",
		"linkedin":                "on",
		"matrix-corporal":         "on",
		"miniflux":                "on",
		"miounne":                 "on",
		"mjolnir":                 "on",
		"radicale":                "on",
		"reminder-bot":            "on",
		"signal":                  "on",
		"skype":                   "on",
		"slack":                   "on",
		"smtp-relay":              "on",
		"stats":                   "on",
		"steam":                   "on",
		"synapse-simple-antispam": "on",
		"synapse-workers":         "on",
		"telegram":                "on",
		"twitter":                 "on",
		"webhooks":                "on",
		"whatsapp":                "on",
		"wireguard":               "on",
	}

	suite.nv = new(mocks.NetworkValidator)
	suite.ext = NewEtkecc(suite.nv).(*etkecc)
	suite.ext.test = true
}

func (suite *EtkeccExtensionSuite) TearDownTest() {
	suite.T().Helper()
}

func (suite *EtkeccExtensionSuite) TestExecuteMarkdown() {
	expected, readErr := ioutil.ReadFile("testdata/etkecc.full.md")
	suite.nv.On("HasA", "example.com").Return(true)
	suite.nv.On("HasCNAME", "example.com").Return(false)

	actual := suite.ext.ExecuteMarkdown(name, suite.orderFull)
	// to generate output
	// os.WriteFile("testdata/etkecc.full.md", []byte(actual), 0o666)

	suite.Require().NoError(readErr)
	suite.Equal(string(expected), actual)
}

func (suite *EtkeccExtensionSuite) TestExecuteMarkdown_Empty() {
	expected, readErr := ioutil.ReadFile("testdata/etkecc.empty.md")

	actual := suite.ext.ExecuteMarkdown(name, suite.orderEmpty)
	// to generate output
	// os.WriteFile("testdata/etkecc.empty.md", []byte(actual), 0o666)

	suite.Require().NoError(readErr)
	suite.Equal(string(expected), actual)
}

func (suite *EtkeccExtensionSuite) TestExecuteHTML() {
	expected := "HTML output is not implemented"

	actual := suite.ext.ExecuteHTML(name, suite.orderFull)

	suite.Equal(expected, actual)
}

func (suite *EtkeccExtensionSuite) TestGeneratePassword() {
	suite.ext.test = false

	// NOTE: that's not a password generator test, because actual password generation
	// handled by crypto/rand package, so here we trying to validate/check the passwordGenerator() wrapper method.
	first := suite.ext.generatePassword()
	second := suite.ext.generatePassword()

	suite.NotEqual(first, second)
}

func TestEtkeccExtensionSuite(t *testing.T) {
	suite.Run(t, new(EtkeccExtensionSuite))
}
