```yaml
Hi there,
we got your order and have some questions before the setup.

I see that you have something on your base domain, in that case you should add following HTTPS redirects (HTTP 301):
* https://example.com/.well-known/matrix/server -> https://matrix.example.com/.well-known/matrix/server
* https://example.com/.well-known/matrix/client -> https://matrix.example.com/.well-known/matrix/client

Reminder bot: What's your timezone (IANA)? Like America/Chicago, Asia/Seoul or Europe/Berlin

Telegram: Please, go to https://my.telegram.org/apps and create a new app. Share the API ID and Hash with me

Matrix email: please, create a matrix mailbox in your migadu admin dashboard and share with me mailbox email and password.

Miniflux: are you sure you want it? It's a RSS reader, not integrated to matrix. Please, check https://miniflux.app and decide.

Languagetool: are you sure you want it? It's an 'opensource grammarly' server, requires ~30GB of disk space for n-grams and not integrated to matrix. Please, check https://languagetool.org and decide.

WireGuard: are you sure you want it? It's a VPN, not integrated to matrix. Please, check https://wireguard.com and decide. If you still want it, please, share with me a list of labels you want to assign to generated client keys (just to set filename, so even '1,2,3...') is OK

Miounne: are you sure you want it? Miounne is a bridge between external services (like html/http forms, matrix-registration, buymeacoffee, etc.) and matrix. Please, check https://gitlab.com/etke.cc/miounne and decide. If you still want it, please, send me a configuration to apply (no, there is no 'default configuration'. No, there is no 'good configuration'. No, we don't provide configuration templates. It's completely up to you)

Corporal: are you sure you want it? Matrix Corporal is a 'kubernetes for matrix', based on policies. It may be suitable for large organizations intranets or large public servers, but it's too complex for private and small servers. I suggest you to skip it, but if you still want it - please, provide a policy that you want to set (no, there is no default policy. No, there is no 'some good policy'. No, we don't provide any policies, that's completely up to you).

Simple antispam: are you sure you want it? It's a synapse module to ban other matrix servers on your HS. It's not suitable for moderation. I suggest you to skip it (because you will have moderation tools directly in Element client and synapse-admin), if you still want it - please, provide a blocklist you want to include (no, there is no default ban list. No, there is no 'some good banlist'. No, we don't provide any banlists, that's completely up to you).

Synapse workers: are you sure you want them? Workers suitable only if your homeserver has 1000+ active real users and require additional compute power to work. I suggest you to skip them.

Go NEB: are you sure you want it? You have go neb by default in Element integration manager, hosted by New Vector. I suggest you to skip it, if you still want it - please, provide a configuration (ref: https://github.com/matrix-org/go-neb#configuring-services) you want to include (no, there is no default configuration. No there is no 'some good configuration'. No, we don't provide any custom configuration, that's completely up to you).

Mjolnir: are you sure you want it? Mjolnir used as automated moderation system on large public homeservers, but for your usecase even standard moderation tools (included in Element client and synapse-admin by default) should be enough. I suggest you to skip it and think about it later when you will be sure that you can't run a server without it.

SSH: please, share with me your public ssh key and your public static IP(-s) to get ssh root access to your server

DNS - please, add following entries:
matrix	A record	server ip
cinny	CNAME record	matrix.example.com
element	CNAME record	matrix.example.com
goneb	CNAME record	matrix.example.com
hydrogen	CNAME record	matrix.example.com
jitsi	CNAME record	matrix.example.com
kuma	CNAME record	matrix.example.com
languagetool	CNAME record	matrix.example.com
miniflux	CNAME record	matrix.example.com
miounne	CNAME record	matrix.example.com
stats	CNAME record	matrix.example.com
radicale	CNAME record	matrix.example.com
```

vars.yml:

```yaml
# system
system_security_autorizedkeys: []

# homeserver https://matrix.example.com
matrix_domain: example.com
matrix_ssl_lets_encrypt_support_email: test@test.com
matrix_ma1sd_enabled: no
matrix_mailer_enabled: no

# synapse
matrix_homeserver_implementation: synapse
matrix_synapse_presence_enabled: yes
matrix_synapse_enable_group_creation: yes
matrix_synapse_max_upload_size_mb: 1024
matrix_synapse_tmp_directory_size_mb: "{{ matrix_synapse_max_upload_size_mb * 2 }}"

# synapse::federation
matrix_synapse_allow_public_rooms_over_federation: yes
matrix_synapse_allow_public_rooms_without_auth: yes

# synapse::mailer
matrix_synapse_email_enabled: yes
matrix_synapse_email_smtp_host: TODO
matrix_synapse_email_smtp_port: 587
matrix_synapse_email_smtp_user: TODO
matrix_synapse_email_smtp_pass: TODO
matrix_synapse_email_notif_from: "Matrix <matrix@{{ matrix_domain }}>"
matrix_synapse_email_client_base_url: "https://{{ matrix_server_fqn_element }}"
matrix_synapse_email_invite_client_location: "https://{{ matrix_server_fqn_element }}"

# synapse::credentials
matrix_synapse_macaroon_secret_key: TODO
matrix_postgres_connection_password: TODO
matrix_synapse_password_config_pepper: TODO
matrix_coturn_turn_static_auth_secret: TODO
matrix_homeserver_generic_secret_key: "{{ matrix_synapse_macaroon_secret_key }}"

# synapse::custom
matrix_synapse_configuration_extension_yaml: |
  disable_msisdn_registration: yes
  allow_device_name_lookup_over_federation: no

# synapse::privacy
matrix_synapse_user_ips_max_age: 5m
matrix_synapse_redaction_retention_period: 5m

# synapse::extensions::shared_secret_auth
matrix_synapse_ext_password_provider_shared_secret_auth_enabled: yes
matrix_synapse_ext_password_provider_shared_secret_auth_shared_secret: TODO

# synapse::extensions::simple-antispam
matrix_synapse_ext_spam_checker_synapse_simple_antispam_enabled: yes
matrix_synapse_ext_spam_checker_synapse_simple_antispam_config_blocked_homeservers: []

# synapse::extensions::spam_checker_mjolnir
matrix_synapse_ext_spam_checker_mjolnir_antispam_enabled: yes
matrix_synapse_ext_spam_checker_mjolnir_antispam_config_block_invites: no
matrix_synapse_ext_spam_checker_mjolnir_antispam_config_block_messages: no
matrix_synapse_ext_spam_checker_mjolnir_antispam_config_block_usernames: no
matrix_synapse_ext_spam_checker_mjolnir_antispam_config_ban_lists: []

# synapse::extensions::rest_auth
matrix_synapse_ext_password_provider_rest_auth_enabled: yes
matrix_synapse_ext_password_provider_rest_auth_endpoint: "http://matrix-corporal:41080/_matrix/corporal"

# synapse::workers
matrix_synapse_workers_enabled: yes
matrix_synapse_workers_preset: one-of-each

# postgres::backups
matrix_postgres_backup_enabled: yes
matrix_postgres_backup_schedule: '@daily'
matrix_postgres_backup_keep_days: 7
matrix_postgres_backup_keep_weeks: 0
matrix_postgres_backup_keep_months: 0

# matrix corporal
matrix_corporal_enabled: yes
matrix_corporal_http_api_enabled: yes
matrix_corporal_http_api_auth_token: TODO
matrix_corporal_corporal_user_id_local_part: "corporal" # password: TODO
matrix_corporal_policy_provider_config: |
  # TODO

# nginx proxy
matrix_nginx_proxy_access_log_enabled: no
matrix_nginx_proxy_base_domain_serving_enabled: no
matrix_nginx_proxy_base_domain_homepage_enabled: no
matrix_ssl_additional_domains_to_obtain_certificates_for:
- "{{ matrix_server_fqn_kuma }}"
- "{{ matrix_server_fqn_languagetool }}"
- "{{ matrix_server_fqn_miniflux }}"
- "{{ matrix_server_fqn_miounne }}"
- "{{ matrix_server_fqn_radicale }}"

# cinny https://cinny.example.com
matrix_client_cinny_enabled: yes

# element https://element.example.com
matrix_client_element_enabled: yes
matrix_client_element_default_theme: dark
matrix_server_fqn_element: "element.{{ matrix_domain }}"

# jitsi https://jitsi.example.com
matrix_jitsi_enabled: yes
matrix_jitsi_jvb_auth_password: TODO
matrix_jitsi_jibri_xmpp_password: TODO
matrix_jitsi_jibri_recorder_password: TODO
matrix_jitsi_jicofo_auth_password: TODO
matrix_jitsi_jicofo_component_secret: TODO

# stats https://stats.example.com
matrix_grafana_enabled: yes
matrix_prometheus_enabled: yes
matrix_grafana_anonymous_access: no
matrix_prometheus_node_exporter_enabled: yes
matrix_grafana_default_admin_user: test
matrix_grafana_default_admin_password: TODO

# synapse-admin https://matrix.example.com/synapse-admin
matrix_synapse_admin_enabled: yes

# dnsmasq
custom_dnsmasq_enabled: yes

# uptime-kuma https://kuma.example.com
custom_kuma_enabled: yes
matrix_server_fqn_kuma: "kuma.{{ matrix_domain }}"

# languagetool https://languagetool.example.com
custom_languagetool_enabled: yes
custom_languagetool_ngrams_enabled: yes # WARNING: requires a LOT of storage
matrix_server_fqn_languagetool: "languagetool.{{ matrix_domain }}"

# miniflux https://miniflux.example.com
custom_miniflux_enabled: yes
custom_miniflux_database_password: TODO
matrix_server_fqn_miniflux: "miniflux.{{ matrix_domain }}"
# TODO:
# 1. SQL:
# CREATE USER miniflux WITH PASSWORD 'TODO';
# CREATE DATABASE miniflux; GRANT ALL PRIVILEGES ON DATABASE miniflux to miniflux;
# 2. Create user:
# docker exec -it custom-miniflux /usr/bin/miniflux -create-admin

# miounne https://miounne.example.com
custom_miounne_enabled: yes
custom_miounne_matrix_user_login: miounne
custom_miounne_matrix_user_password: TODO
# TODO: only for registration
custom_miounne_registration_url: https://matrix.example.com/matrix-registration
custom_miounne_matrix_registration_room: TODO
custom_miounne_matrix_registration_secret: TODO
# TODO: only for BMC
custom_miounne_bmc_token: TODO
custom_miounne_bmc_room: TODO
custom_miounne_bmc_notify_extras: 1
custom_miounne_bmc_notify_members: 1
custom_miounne_bmc_notify_supporters: 1
# TODO: only for forms
matrix_server_fqn_miounne: "miounne.{{ matrix_domain }}"
matrix_nginx_proxy_proxy_miounne_hostname: "{{ matrix_server_fqn_miounne }}"
custom_miounne_spam_emails: []
custom_miounne_spam_hosts: []
custom_miounne_forms: []
# TODO: matrix-synapse-register-user miounne TODO 0

# radicale https://radicale.example.com
custom_radicale_enabled: yes
matrix_server_fqn_radicale: "radicale.{{ matrix_domain }}"
custom_radicale_htpasswd: "test:TODO"
# TODO:
# 1. htpasswd -nb test TODO
# 2. add the password to onboarding list
# wireguard
custom_wireguard_enabled: yes
custom_wireguard_overwrite: no
custom_wireguard_clients: [] # TODO

# bots::goneb
matrix_bot_go_neb_enabled: yes
matrix_bot_go_neb_clients:
- UserID: "@goneb:{{ matrix_domain }}"
  AccessToken: "TODO" # password: TODO
  DeviceID: server
  HomeserverURL: "{{ matrix_homeserver_container_url }}"
  Sync: yes
  AutoJoinRooms: yes
  DisplayName: "GoNEB"
  AcceptVerificationFromUsers: [":{{ matrix_domain }}"]
matrix_bot_go_neb_services: [] # TODO

# bots::mjolnir
matrix_bot_mjolnir_enabled: yes
matrix_bot_mjolnir_access_token: "TODO" # password: TODO
matrix_bot_mjolnir_management_room: TODO
matrix_bot_mjolnir_configuration_extension_yaml: |
  recordIgnoredInvites: true

# bots::reminder
matrix_bot_matrix_reminder_bot_enabled: yes
matrix_bot_matrix_reminder_bot_reminders_timezone: TODO
matrix_bot_matrix_reminder_bot_matrix_user_id_localpart: reminder
matrix_bot_matrix_reminder_bot_matrix_user_password: TODO
# TODO: matrix-synapse-register-user reminder TODO 0

# bridges::discord
matrix_mx_puppet_discord_enabled: yes
matrix_mx_puppet_discord_configuration_extension_yaml: |
  presence:
    enabled: no
  logging:
    console: warn

# bridges::facebook
matrix_mautrix_facebook_enabled: yes
matrix_mautrix_facebook_bridge_presence: no
matrix_mautrix_facebook_configuration_extension_yaml: |
  bridge:
    permissions:
      "{{ matrix_mautrix_facebook_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      paho:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::googlechat
matrix_mautrix_googlechat_enabled: yes
matrix_mautrix_googlechat_bridge_presence: no
matrix_mautrix_googlechat_configuration_extension_yaml: |
  bridge:
    permissions:
      "{{ matrix_mautrix_googlechat_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      hangups:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::groupme
matrix_mx_puppet_groupme_enabled: yes
matrix_mx_puppet_groupme_configuration_extension_yaml: |
  presence:
    enabled: no
  logging:
    console: warn

# bridges::irc (heisenbridge)
matrix_heisenbridge_enabled: yes
matrix_heisenbridge_identd_enabled: yes
matrix_heisenbridge_owner: "@test:{{ matrix_domain }}"

# bridges::instagram
matrix_mautrix_instagram_enabled: yes
matrix_mautrix_instagram_bridge_presence: no
matrix_mautrix_instagram_configuration_extension_yaml: |
  bridge:
    permissions:
      "{{ matrix_mautrix_instagram_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      mauigpapi:
        level: WARNING
      paho:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::linkedin
matrix_beeper_linkedin_enabled: yes
matrix_beeper_linkedin_bridge_presence: no
matrix_beeper_linkedin_configuration_extension_yaml: |
  bridge:
    permissions:
      "{{ matrix_beeper_linkedin_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      paho:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::signal
matrix_mautrix_signal_enabled: yes
matrix_mautrix_signal_bridge_presence: no
matrix_mautrix_signal_configuration_extension_yaml: |
  bridge:
    permissions:
      "{{ matrix_mautrix_signal_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::skype
matrix_mx_puppet_skype_enabled: yes
matrix_mx_puppet_skype_configuration_extension_yaml: |
  presence:
    enabled: no
  logging:
    console: warn

# bridges::slack
matrix_mx_puppet_slack_enabled: yes
matrix_mx_puppet_slack_configuration_extension_yaml: |
  presence:
    enabled: no
  logging:
    console: warn

# bridges::steam
matrix_mx_puppet_steam_enabled: yes
matrix_mx_puppet_steam_configuration_extension_yaml: |
  presence:
    enabled: no
  logging:
    console: warn

# bridges::telegram
matrix_mautrix_telegram_enabled: yes
matrix_mautrix_telegram_api_id: TODO
matrix_mautrix_telegram_api_hash: TODO
matrix_mautrix_telegram_bridge_presence: no
matrix_mautrix_telegram_configuration_extension_yaml: |
  bridge:
    delivery_error_reports: yes
    delivery_receipts: no
    max_initial_member_sync: 10
    private_chat_portal_meta: yes
    sync_channel_members: no
    permissions:
      "{{ matrix_mautrix_telegram_homeserver_domain }}": full
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      telethon:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::twitter
matrix_mautrix_twitter_enabled: yes
matrix_mautrix_twitter_bridge_presence: no
matrix_mautrix_twitter_configuration_extension_yaml: |
  bridge:
    delivery_error_reports: yes
    delivery_receipts: no
    private_chat_portal_meta: yes
    permissions:
      "{{ matrix_mautrix_twitter_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    loggers:
      mau:
        level: WARNING
      aiohttp:
        level: WARNING
    root:
      level: WARNING
      handlers: [console]

# bridges::webhooks
matrix_appservice_webhooks_enabled: yes
matrix_appservice_webhooks_api_secret: TODO

# bridges::whatsapp
matrix_mautrix_whatsapp_enabled: yes
matrix_mautrix_whatsapp_bridge_presence: no
matrix_mautrix_whatsapp_configuration_extension_yaml: |
  bridge:
    permissions:
      "{{ matrix_mautrix_whatsapp_homeserver_domain }}": user
      "@test:{{ matrix_domain }}": admin
  logging:
    print_level: warn
```

onboarding:

```md
# links

* homeserver: https://matrix.example.com
* synapse-admin: https://matrix.example.com/synapse-admin
* cinny: https://cinny.example.com
* element: https://element.example.com
* hydrogen: https://hydrogen.example.com
* jitsi: https://jitsi.example.com
* uptime-kuma: https://kuma.example.com
* languagetool: https://languagetool.example.com
* miniflux: https://miniflux.example.com
* miounne: https://miounne.example.com
* stats: https://stats.example.com
* radicale: https://radicale.example.com

# bots

* go-neb: @goneb:example.com
* miounne: @miounne:example.com
* mjolnir: @mjolnir:example.com
* reminder: @reminder:example.com

# bridges

* discord: @_discordpuppet_bot:example.com
* facebook: @facebookbot:example.com
* google chat: @googlechatbot:example.com
* groupme: @_groupmepuppet_bot:example.com
* instagram: @instagrambot:example.com
* irc: @heisenbridge:example.com
* linkedin: @linkedinbot:example.com
* signal: @signalbot:example.com
* skype: @_skypepuppet_bot:example.com
* slack: @_slackpuppet_bot:example.com
* steam: @_steampuppet_bot:example.com
* telegram: @telegrambot:example.com
* twitter: @twitterbot:example.com
* whatsapp: @whatsappbot:example.com

 > You can find instructions for any bridge here: https://etke.cc/bridges

# credentials

* login: test
* password: TODO
* radicale password: TODO
* mxid: @test:example.com

In case of any issues: @support:etke.cc

> **NOTE**: don't forget to change the password as soon as you login for the first time!
```

