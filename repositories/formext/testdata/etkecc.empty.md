```yaml
Hi there,
we got your order and have some questions before the setup.

SMTP relay: please, select suitable email provider (big providers like Gmail or Outlook will ban you for automated mails, so you need to find a service that allows sending of verification emails. Optionally, we provide such service). Please, send me an SMTP host, SMTP STARTTLS port, SMTP login, SMTP password, SMTP email (usually login and email is the same thing, but that depends on provider).

WireGuard: are you sure you want it (and without dnsmasq)? It's a VPN, not integrated to matrix. And dnsmasq is a caching recursive resolver, works best inside wireguard tunnels. Please, check https://wireguard.com and decide. If you still want it, please, share with me a list of labels you want to assign to generated client keys (just to set filename, so even '1,2,3...') is OK

Prometheus+Grafana: are you sure you want it? Any cloud provider gives you some dashboard with server stats, why not use that dashboard? Prometheus+Grafana stack provides some internal matrix stats (like count of events), but it's overkill if you just want to see server utilization. I suggest you to skip it.

Server: please, create a VPS with any debian-based distro. Minimal comfortable configuration for a basic matrix server: 1vCPU, 2GB RAM.
Add my ssh key (https://etke.cc/ssh.key) to your server, share with me your server IP, username (with permissions to call sudo) and password (if set).

DNS - please, add following entries:
@	A record	server ip
matrix	A record	server ip
stats	CNAME record	matrix.TODO.com
```

vars.yml:

```yaml
# system
system_security_autorizedkeys: []

# homeserver https://matrix.TODO.com
matrix_domain: TODO.com
matrix_ssl_lets_encrypt_support_email: test@test.com
matrix_ma1sd_enabled: no
matrix_mailer_enabled: no

# synapse
matrix_homeserver_implementation: synapse
matrix_synapse_presence_enabled: yes
matrix_synapse_enable_group_creation: yes
matrix_synapse_max_upload_size_mb: 1024
matrix_synapse_tmp_directory_size_mb: "{{ matrix_synapse_max_upload_size_mb * 2 }}"

# synapse::federation
matrix_synapse_allow_public_rooms_over_federation: yes
matrix_synapse_allow_public_rooms_without_auth: yes

# synapse::mailer
matrix_synapse_email_enabled: yes
matrix_synapse_email_smtp_host: TODO
matrix_synapse_email_smtp_port: 587
matrix_synapse_email_smtp_user: TODO
matrix_synapse_email_smtp_pass: TODO
matrix_synapse_email_notif_from: "Matrix <matrix@{{ matrix_domain }}>"
matrix_synapse_email_client_base_url: "https://{{ matrix_server_fqn_element }}"
matrix_synapse_email_invite_client_location: "https://{{ matrix_server_fqn_element }}"

# synapse::credentials
matrix_synapse_macaroon_secret_key: TODO
matrix_postgres_connection_password: TODO
matrix_synapse_password_config_pepper: TODO
matrix_coturn_turn_static_auth_secret: TODO
matrix_homeserver_generic_secret_key: "{{ matrix_synapse_macaroon_secret_key }}"

# synapse::custom
matrix_synapse_configuration_extension_yaml: |
  disable_msisdn_registration: yes
  allow_device_name_lookup_over_federation: no

# synapse::privacy
matrix_synapse_user_ips_max_age: 5m
matrix_synapse_redaction_retention_period: 5m

# synapse::extensions::shared_secret_auth
matrix_synapse_ext_password_provider_shared_secret_auth_enabled: yes
matrix_synapse_ext_password_provider_shared_secret_auth_shared_secret: TODO

# dendrite
matrix_homeserver_implementation: dendrite

# postgres::backups
matrix_postgres_backup_enabled: yes
matrix_postgres_backup_schedule: '@daily'
matrix_postgres_backup_keep_days: 7
matrix_postgres_backup_keep_weeks: 0
matrix_postgres_backup_keep_months: 0

# nginx proxy
matrix_nginx_proxy_access_log_enabled: no
matrix_nginx_proxy_base_domain_serving_enabled: yes
matrix_nginx_proxy_base_domain_homepage_enabled: no

# stats https://stats.TODO.com
matrix_grafana_enabled: yes
matrix_prometheus_enabled: yes
matrix_grafana_anonymous_access: no
matrix_prometheus_node_exporter_enabled: yes
matrix_grafana_default_admin_user: TODO
matrix_grafana_default_admin_password: TODO

# synapse-admin https://matrix.TODO.com/synapse-admin
matrix_synapse_admin_enabled: yes

# wireguard
custom_wireguard_enabled: yes
custom_wireguard_overwrite: no
custom_wireguard_clients: [] # TODO
```

onboarding:

```md
# links

* homeserver: https://matrix.TODO.com
* synapse-admin: https://matrix.TODO.com/synapse-admin
* stats: https://stats.TODO.com

# credentials

* login: TODO
* password: TODO
* mxid: @TODO:TODO.com

In case of any issues: @support:etke.cc

> **NOTE**: don't forget to change the password as soon as you login for the first time!

# payment

Please, [buy the Setup item](https://etke.cc/setup).

If you want an ongoing maintenance of your server (host/system maintenance, matrix components maintenance, updates and reconfiguration),
join the **Maintenance** membership on [https://etke.cc/membership](https://etke.cc/membership).
Please, clarify now if you want to join the maintenance membership, because if not - we will remove any your configuration and credentials from our side.

> **NOTE**: all prices are based on [Pay What You Want](https://en.wikipedia.org/wiki/Pay_what_you_want) model with minimal (floor) price set
```

