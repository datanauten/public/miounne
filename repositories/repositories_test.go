package repositories

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type RepositoriesSuite struct {
	suite.Suite
}

func (suite *RepositoriesSuite) SetupTest() {
	suite.T().Helper()
}

func TestRepositoriesSuite(t *testing.T) {
	suite.Run(t, new(RepositoriesSuite))
}
