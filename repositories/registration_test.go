package repositories

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/models"
)

type RegistrationSuite struct {
	suite.Suite
}

func (suite *RegistrationSuite) TestNew() {
	roomID := id.RoomID("!test@example.com")

	client := NewRegistration("https://example.com", "test", roomID, false, false)

	suite.Equal(roomID, client.GetRoomID())
}

func (suite *RegistrationSuite) TestReadError() {
	client := NewRegistration("https://example.com", "test", id.RoomID("!test@test.com"), false, false)

	tests := []struct {
		name string
		test []byte
		code int
		err  error
	}{
		{
			name: "error",
			code: 400,
			test: []byte(`{"errcode": "TEST", "error":"test error"}`),
			err:  fmt.Errorf("TEST: test error"),
		},
		{
			name: "valid",
			code: 200,
			test: []byte(`{"status":"ok"}`),
			err:  nil,
		},
		{
			name: "no backend",
			code: 404,
			test: []byte(`<h1>404 not found</h1>`),
			err:  fmt.Errorf("matrix-registration not found, check the API url"),
		},
		{
			name: "empty",
			code: 400,
			test: []byte(``),
			err:  nil,
		},
		{
			name: "invalid json",
			code: 400,
			test: []byte(`404 page not found`),
			err:  nil,
		},
	}

	for _, test := range tests {
		suite.Run(test.name, func() {
			err := client.readError(test.code, test.test)

			suite.Equal(test.err, err)
		})
	}
}

func (suite *RegistrationSuite) TestGetStatus() {
	tests := []struct {
		Name    string
		Handler http.HandlerFunc
		Token   *models.RegistrationToken
		Error   error
	}{
		{
			Name: "valid token",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"name":"test",
					"valid":true,
					"disabled":false,
					"max_usage":1,
					"used":0,
					"expiration_date":""
				}`))
			},
			Token: &models.RegistrationToken{
				Name:     "test",
				Valid:    true,
				Disabled: false,
				MaxUsage: 1,
				Used:     0,
				Expires:  "",
			},
			Error: nil,
		},
		{
			Name: "api error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"errcode": "TEST",
					"error": "test error"
				}`))
			},
			Token: nil,
			Error: fmt.Errorf("TEST: test error"),
		},
		// that tests uses a hacky way to get json.SyntaxError{}, because it contains private fields (and they must be set)
		{
			Name: "json error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`404 page not found`))
			},
			Token: nil,
			Error: json.Unmarshal([]byte(`404 page not found`), &models.RegistrationToken{}),
		},
	}

	for _, test := range tests {
		server := httptest.NewServer(test.Handler)
		defer server.Close()

		client := NewRegistration(server.URL, "test", id.RoomID("!test@test.com"), false, false)
		token, err := client.GetStatus("test")

		suite.Equal(test.Token, token)
		suite.Equal(test.Error, err)
	}
}

func (suite *RegistrationSuite) TestGetList() {
	tests := []struct {
		Name    string
		Handler http.HandlerFunc
		Tokens  []models.RegistrationToken
		Error   error
	}{
		{
			Name: "valid tokens",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`[
					{
						"name":"test",
						"valid":true,
						"disabled":false,
						"max_usage":1,
						"used":0,
						"expiration_date":""
					},
					{
						"name":"test2",
						"valid":true,
						"disabled":false,
						"max_usage":1,
						"used":0,
						"expiration_date":""
					}
				]`))
			},
			Tokens: []models.RegistrationToken{
				{
					Name:     "test",
					Valid:    true,
					Disabled: false,
					MaxUsage: 1,
					Used:     0,
					Expires:  "",
				},
				{
					Name:     "test2",
					Valid:    true,
					Disabled: false,
					MaxUsage: 1,
					Used:     0,
					Expires:  "",
				},
			},
			Error: nil,
		},
		{
			Name: "api error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"errcode": "TEST",
					"error": "test error"
				}`))
			},
			Tokens: []models.RegistrationToken(nil),
			Error:  fmt.Errorf("TEST: test error"),
		},
		// that tests uses a hacky way to get json.SyntaxError{}, because it contains private fields (and they must be set)
		{
			Name: "json error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`404 page not found`))
			},
			Tokens: []models.RegistrationToken(nil),
			Error:  json.Unmarshal([]byte(`404 page not found`), &models.RegistrationToken{}),
		},
	}

	for _, test := range tests {
		server := httptest.NewServer(test.Handler)
		defer server.Close()

		client := NewRegistration(server.URL, "test", id.RoomID("!test@test.com"), false, false)
		tokens, err := client.GetList()

		suite.ElementsMatch(test.Tokens, tokens)
		suite.Equal(test.Error, err)
	}
}

func (suite *RegistrationSuite) TestDisableToken() {
	tests := []struct {
		Name    string
		Handler http.HandlerFunc
		Token   *models.RegistrationToken
		Error   error
	}{
		{
			Name: "valid token",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				// emulate older version
				if r.Method == "PATCH" {
					http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
					return
				}
				w.Write([]byte(`{
					"name":"test",
					"valid":true,
					"disabled":false,
					"max_usage":1,
					"used":0,
					"expiration_date":""
				}`))
			},
			Token: &models.RegistrationToken{
				Name:     "test",
				Valid:    true,
				Disabled: false,
				MaxUsage: 1,
				Used:     0,
				Expires:  "",
			},
			Error: nil,
		},
		{
			Name: "api error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"errcode": "TEST",
					"error": "test error"
				}`))
			},
			Token: nil,
			Error: fmt.Errorf("TEST: test error"),
		},
		// that tests uses a hacky way to get json.SyntaxError{}, because it contains private fields (and they must be set)
		{
			Name: "json error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`404 page not found`))
			},
			Token: nil,
			Error: json.Unmarshal([]byte(`404 page not found`), &models.RegistrationToken{}),
		},
	}

	for _, test := range tests {
		server := httptest.NewServer(test.Handler)
		defer server.Close()

		client := NewRegistration(server.URL, "test", id.RoomID("!test@test.com"), false, false)
		token, err := client.DisableToken("test")

		suite.Equal(test.Token, token)
		suite.Equal(test.Error, err)
	}
}

func (suite *RegistrationSuite) TestCreateToken() {
	tests := []struct {
		Name           string
		MaxUsage       string
		ExpirationDate string
		Handler        http.HandlerFunc
		Token          *models.RegistrationToken
		Error          error
	}{
		{
			Name:     "valid token, once",
			MaxUsage: "1",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"name":"test",
					"valid":true,
					"disabled":false,
					"max_usage":1,
					"used":0,
					"expiration_date":""
				}`))
			},
			Token: &models.RegistrationToken{
				Name:     "test",
				Valid:    true,
				Disabled: false,
				MaxUsage: 1,
				Used:     0,
				Expires:  "",
			},
			Error: nil,
		},
		{
			Name:     "valid token, invalid max usage",
			MaxUsage: "invalid",
			Handler:  func(w http.ResponseWriter, r *http.Request) {},
			Token:    nil,
			Error:    &strconv.NumError{Func: "Atoi", Num: "invalid", Err: fmt.Errorf("invalid syntax")},
		},
		{
			Name:           "valid token, expires",
			ExpirationDate: "1999-12-12",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"name":"test",
					"valid":true,
					"disabled":false,
					"max_usage":0,
					"used":0,
					"expiration_date":"1999-12-12"
				}`))
			},
			Token: &models.RegistrationToken{
				Name:     "test",
				Valid:    true,
				Disabled: false,
				MaxUsage: 0,
				Used:     0,
				Expires:  "1999-12-12",
			},
			Error: nil,
		},
		{
			Name:           "valid token, invalid date",
			ExpirationDate: "invalid",
			Handler:        func(w http.ResponseWriter, r *http.Request) {},
			Token:          nil,
			Error:          &time.ParseError{Layout: "2006-01-02", Value: "invalid", LayoutElem: "2006", ValueElem: "invalid", Message: ""},
		},
		{
			Name: "api error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
					"errcode": "TEST",
					"error": "test error"
				}`))
			},
			Token: nil,
			Error: fmt.Errorf("TEST: test error"),
		},
		// that tests uses a hacky way to get json.SyntaxError{}, because it contains private fields (and they must be set)
		{
			Name: "json error",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`404 page not found`))
			},
			Token: nil,
			Error: json.Unmarshal([]byte(`404 page not found`), &models.RegistrationToken{}),
		},
	}

	for _, test := range tests {
		server := httptest.NewServer(test.Handler)
		defer server.Close()

		client := NewRegistration(server.URL, "test", id.RoomID("!test@test.com"), false, false)
		token, err := client.CreateToken(test.MaxUsage, test.ExpirationDate)

		suite.Equal(test.Token, token)
		suite.Equal(test.Error, err)
	}
}

func (suite *RegistrationSuite) SetupTest() {
	suite.T().Helper()
}

func TestMatrixRegistrationSuite(t *testing.T) {
	suite.Run(t, new(RegistrationSuite))
}
