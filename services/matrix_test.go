package services

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

type MatrixClientSuite struct {
	suite.Suite
	userID   id.UserID
	roomID   id.RoomID
	deviceID id.DeviceID
	password string
}

func (suite *MatrixClientSuite) SetupTest() {
	suite.T().Helper()

	suite.userID = id.UserID("@test:example.com")
	suite.roomID = id.RoomID("!test:example.com")
	suite.deviceID = id.DeviceID("test")
	suite.password = "test"
}

func (suite *MatrixClientSuite) newClient() *mautrix.Client {
	response := `{
		"access_token": "` + suite.deviceID.String() + `",
		"device_id": "` + suite.deviceID.String() + `",
		"user_id": "` + suite.userID.String() + `"
	}`
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, request *http.Request) {
		w.Write([]byte(response))
	}))

	client, _, err := NewMatrixClient(server.URL, suite.userID.String(), suite.password)

	suite.Require().NoError(err)
	return client.(*mautrix.Client)
}

func (suite *MatrixClientSuite) TestNewMatrixClient() {
	client := suite.newClient()

	suite.Equal(suite.userID, client.UserID)
	suite.Equal(suite.deviceID, client.DeviceID)
}

func (suite *MatrixClientSuite) TestNewMatrixRegistrationClient() {
	client := NewMatrixRegistrationClient("https://example.com", "secret", suite.roomID, true, false)

	suite.Equal(suite.roomID, client.GetRoomID())
	suite.True(client.GetNotifyNew())
	suite.False(client.GetNotifyUse())
}

func TestMatrixClientSuite(t *testing.T) {
	suite.Run(t, new(MatrixClientSuite))
}
