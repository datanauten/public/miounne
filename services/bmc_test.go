package services

import (
	"errors"
	"testing"

	"git.sr.ht/~xn/bmc"
	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/mocks"
	"gitlab.com/etke.cc/miounne/v2/models"
)

type BMCSuite struct {
	suite.Suite
	api    *mocks.BMCAPIclient
	bmc    *bmclient
	policy *mocks.Policy
	roomID id.RoomID
}

func (suite *BMCSuite) SetupTest() {
	suite.T().Helper()
	suite.policy = new(mocks.Policy)
	suite.roomID = id.RoomID("!test:example.com")
	suite.api = new(mocks.BMCAPIclient)
	suite.bmc = NewBMCClient(suite.api, suite.roomID, suite.policy, false, false, false).(*bmclient)
}

func (suite *BMCSuite) TearDownTest() {
	suite.T().Helper()
	suite.api.AssertExpectations(suite.T())
	suite.policy.AssertExpectations(suite.T())
}

func (suite *BMCSuite) TestNewBMCClient() {
	_, ok := NewBMCClient(suite.api, suite.roomID, suite.policy, false, false, false).(*bmclient)

	suite.True(ok)
}

func (suite *BMCSuite) TestGetRoomID() {
	expected := suite.roomID

	actual := suite.bmc.GetRoomID()

	suite.Equal(expected, actual)
}

func (suite *BMCSuite) TestGetMembers() {
	bmcmembers := bmc.Members{
		{
			Country:                           "",
			IsManualPayout:                    0,
			MembershipLevelID:                 5,
			MessageVisibility:                 1,
			PayerEmail:                        "test@example.com",
			PayerName:                         "test",
			Refererr:                          "",
			SubscriptionCanceledOn:            "",
			SubscriptionCoffeeNum:             1,
			SubscriptionCoffeePrice:           "2.990",
			SubscriptionCreatedOn:             "2020-07-29 15:15:22",
			SubscriptionCurrency:              "USD",
			SubscriptionCurrentPeriodEnd:      "2020-08-29 15:15:18",
			SubscriptionCurrentPeriodStart:    "2020-07-29 15:15:18",
			SubscriptionDurationType:          "month",
			SubscriptionHidden:                0,
			SubscriptionID:                    10647,
			SubscriptionIsCanceled:            0,
			SubscriptionIsCanceledAtPeriodEnd: false,
			SubscriptionMessage:               "",
			SubscriptionUpdatedOn:             "2020-07-29 15:15:22",
			TransactionID:                     "sub_HjkUcn*******",
		},
	}
	expected := []*models.BMCMember{
		{
			ID:         10647,
			LevelID:    5,
			Duration:   "month",
			Price:      2.99,
			Quantity:   1,
			Currency:   "USD",
			Name:       "test",
			Email:      "test@example.com",
			IsCanceled: false,
			CreatedAt:  "2020-07-29 15:15:22",
			UpdatedAt:  "2020-07-29 15:15:22",
			CanceledAt: "",
		},
	}
	suite.api.On("GetMembers", "all").Return(bmcmembers, nil)
	suite.policy.On("Sanitize", "month").Return("month")
	suite.policy.On("Sanitize", "2.990").Return("2.990")
	suite.policy.On("Sanitize", "USD").Return("USD")
	suite.policy.On("Sanitize", "test").Return("test")
	suite.policy.On("Sanitize", "test@example.com").Return("test@example.com")
	suite.policy.On("Sanitize", "2020-07-29 15:15:22").Return("2020-07-29 15:15:22")
	suite.policy.On("Sanitize", "").Return("")

	actual, err := suite.bmc.GetMembers("all")

	suite.NoError(err)
	suite.Equal(expected, actual)
}

func (suite *BMCSuite) TestGetMembers_Error() {
	suite.api.On("GetMembers", "all").Return(nil, errors.New("test"))

	actual, err := suite.bmc.GetMembers("all")

	suite.Error(err)
	suite.Empty(actual)
	suite.Equal(errors.New("test"), err)
}

func (suite *BMCSuite) TestGetSupporters() {
	bmcsupporters := bmc.Supporters{
		{
			Country:            "IN",
			IsRefunded:         0,
			PayerEmail:         "test@example.org",
			PayerName:          "test user",
			PaymentPlatform:    "paypal",
			Refererr:           "",
			SupportCoffeePrice: "3.5400",
			SupportCoffees:     5,
			SupportCreatedOn:   "2020-08-05 09:34:43",
			SupportCurrency:    "GBP",
			SupportEmail:       "test@example.com",
			SupportID:          245730,
			SupportNote:        "Heya!",
			SupportNotePinned:  0,
			SupportUpdatedOn:   "2020-08-05 09:34:43",
			SupportVisibility:  1,
			SupporterName:      "Test User",
			TransactionID:      "7S721018AR89XXXX",
			TransferID:         "",
		},
	}
	expected := []*models.BMCSupporter{
		{
			ID:         245730,
			Price:      3.54,
			Quantity:   5,
			Currency:   "GBP",
			Name:       "Test User",
			Email:      "test@example.com",
			Message:    "Heya!",
			IsRefunded: false,
			CreatedAt:  "2020-08-05 09:34:43",
			UpdatedAt:  "2020-08-05 09:34:43",
		},
	}
	suite.api.On("GetSupporters").Return(bmcsupporters, nil)
	suite.policy.On("Sanitize", "3.5400").Return("3.5400")
	suite.policy.On("Sanitize", "GBP").Return("GBP")
	suite.policy.On("Sanitize", "Test User").Return("Test User")
	suite.policy.On("Sanitize", "test@example.com").Return("test@example.com")
	suite.policy.On("Sanitize", "Heya!").Return("Heya!")
	suite.policy.On("Sanitize", "2020-08-05 09:34:43").Return("2020-08-05 09:34:43")

	actual, err := suite.bmc.GetSupporters()

	suite.NoError(err)
	suite.Equal(expected, actual)
}

func (suite *BMCSuite) TestGetSupporters_Error() {
	suite.api.On("GetSupporters").Return(nil, errors.New("test"))

	actual, err := suite.bmc.GetSupporters()

	suite.Error(err)
	suite.Empty(actual)
	suite.Equal(errors.New("test"), err)
}

func (suite *BMCSuite) TestGetExtras() {
	bmcextras := bmc.Extras{
		{
			Extra: bmc.ExtraItem{
				RewardID:                  1,
				RewardTitle:               "AMA",
				RewardDescription:         "Ask me anything",
				RewardConfirmationMessage: "Join your slot here on zoom: link",
				RewardQuestion:            "share your email?",
				RewardUsed:                3,
				RewardCreatedOn:           "2020-05-19 10:09:29",
				RewardUpdatedOn:           "2020-05-19 10:09:29",
				RewardDeletedOn:           "",
				RewardIsActive:            1,
				RewardImage:               "https://cdn.buymeacoffee.com/uploads/project_updates/2020/05/6cf05f963a4896bab8e377f49da20c8c.jpg",
				RewardSlots:               10,
				RewardCoffeePrice:         "1.00",
				RewardOrder:               0,
			},
			PayerEmail:        "example@example.com",
			PayerName:         "",
			PurchaseAmount:    "1.54",
			PurchaseCurrency:  "USD",
			PurchaseID:        12,
			PurchaseIsRevoked: 0,
			PurchaseQuestion:  "share your email?",
			PurchaseUpdatedOn: "2020-05-20 08:27:34",
			PurchasedOn:       "2020-05-20 08:27:34",
		},
	}
	expected := []*models.BMCPurchase{
		{
			ID:        12,
			Title:     "AMA",
			Price:     1.54,
			Currency:  "USD",
			Name:      "",
			Email:     "example@example.com",
			Message:   "share your email?",
			IsRevoked: false,
			CreatedAt: "2020-05-20 08:27:34",
			UpdatedAt: "2020-05-20 08:27:34",
		},
	}
	suite.api.On("GetExtras").Return(bmcextras, nil)
	suite.policy.On("Sanitize", "AMA").Return("AMA")
	suite.policy.On("Sanitize", "1.54").Return("1.54")
	suite.policy.On("Sanitize", "USD").Return("USD")
	suite.policy.On("Sanitize", "").Return("")
	suite.policy.On("Sanitize", "example@example.com").Return("example@example.com")
	suite.policy.On("Sanitize", "share your email?").Return("share your email?")
	suite.policy.On("Sanitize", "2020-05-20 08:27:34").Return("2020-05-20 08:27:34")
	suite.policy.On("Sanitize", "2020-05-20 08:27:34").Return("2020-05-20 08:27:34")

	actual, err := suite.bmc.GetExtras()

	suite.NoError(err)
	suite.Equal(expected, actual)
}

func (suite *BMCSuite) TestGetExtras_Error() {
	suite.api.On("GetExtras").Return(nil, errors.New("test"))

	actual, err := suite.bmc.GetExtras()

	suite.Error(err)
	suite.Empty(actual)
	suite.Equal(errors.New("test"), err)
}

func TestBMCSuite(t *testing.T) {
	suite.Run(t, new(BMCSuite))
}
