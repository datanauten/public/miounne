package services

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/mocks"
	"gitlab.com/etke.cc/miounne/v2/models"
)

type MatrixBotSuite struct {
	suite.Suite
	roomID       id.RoomID
	userID       id.UserID
	text         string
	client       *mocks.MatrixClient
	syncer       *mocks.MatrixSyncer
	logger       *mocks.Logger
	registration *mocks.MatrixRegistrationClient
	bmc          *mocks.BMC
	bot          *bot
}

func (suite *MatrixBotSuite) SetupTest() {
	suite.T().Helper()

	suite.roomID = id.RoomID("!test:example.com")
	suite.userID = id.UserID("@test:example.com")
	suite.text = "Test"
	suite.syncer = new(mocks.MatrixSyncer)
	suite.client = new(mocks.MatrixClient)
	suite.logger = new(mocks.Logger)
	suite.registration = new(mocks.MatrixRegistrationClient)
	suite.bmc = new(mocks.BMC)

	suite.bot = NewMatrixBot(
		suite.client,
		suite.syncer,
		suite.logger,
		10,
		suite.registration,
		suite.bmc,
	).(*bot)
}

func (suite *MatrixBotSuite) TestNewMatrixBot() {
	evt := event.Type{Type: "m.room.message", Class: 1}
	content := &event.MessageEventContent{MsgType: "m.text", Body: suite.text}
	suite.client.On("SendMessageEvent", suite.roomID, evt, content).Return(nil, nil)

	bot := NewMatrixBot(
		suite.client,
		suite.syncer,
		suite.logger,
		10,
		suite.registration,
		suite.bmc,
	)

	err := bot.SendMessage(suite.roomID, suite.text)

	suite.NoError(err)
}

func (suite *MatrixBotSuite) TestSendMessage() {
	evt := event.Type{Type: "m.room.message", Class: 1}
	content := &event.MessageEventContent{MsgType: "m.text", Body: suite.text}
	suite.client.On("SendMessageEvent", suite.roomID, evt, content).Return(nil, nil)

	err := suite.bot.SendMessage(suite.roomID, suite.text)

	suite.NoError(err)
}

func (suite *MatrixBotSuite) TestSendMessage_Shrink() {
	evt := event.Type{Type: "m.room.message", Class: 1}
	note := "\n\n> **NOTE**: that message was shrunk, because original size is more than allowed"
	message := strings.Repeat("t", MatrixMaxPayloadSize)
	shrinkpoint := len(message)*2 - len(note) - MatrixInfrastructurePayloadSize - MatrixMaxPayloadSize
	shrinkedMessage := message[shrinkpoint:] + note
	content := &event.MessageEventContent{MsgType: "m.text", Body: shrinkedMessage}
	suite.client.On("SendMessageEvent", suite.roomID, evt, content).Return(nil, nil)

	err := suite.bot.SendMessage(suite.roomID, message)

	suite.NoError(err)
}

func (suite *MatrixBotSuite) TestSendMessage_Error() {
	evt := event.Type{Type: "m.room.message", Class: 1}
	content := &event.MessageEventContent{MsgType: "m.text", Body: suite.text}
	suite.client.On("SendMessageEvent", suite.roomID, evt, content).Return(nil, errors.New("SendMessageEvent error"))

	err := suite.bot.SendMessage(suite.roomID, suite.text)

	suite.Error(err)
	suite.Equal(errors.New("SendMessageEvent error"), err)
}

func (suite *MatrixBotSuite) TestSendWebhook() {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, request *http.Request) {
		w.Write([]byte(`{"success": true}`))
	}))

	err := suite.bot.SendWebhook("https://example.com/logo.png", suite.text, "html", "<h1>test</h1>", server.URL)

	suite.NoError(err)
}

func (suite *MatrixBotSuite) TestSendWebhook_JsonError() {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, request *http.Request) {
		w.Write([]byte(`"success": true`))
	}))

	err := suite.bot.SendWebhook("https://example.com/logo.png", suite.text, "html", "<h1>test</h1>", server.URL)

	suite.Error(err)
}

func (suite *MatrixBotSuite) TestSendWebhook_ResponseError() {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, request *http.Request) {
		w.Write([]byte(`{"success": false}`))
	}))

	err := suite.bot.SendWebhook("https://example.com/logo.png", suite.text, "html", "<h1>test</h1>", server.URL)

	suite.Error(err)
}

func (suite *MatrixBotSuite) TestStart() {
	expected := suite.text
	suite.client.On("Whoami").Return(&mautrix.RespWhoami{UserID: "@test:example.com", DeviceID: id.DeviceID(suite.text)}, nil)
	suite.client.On("GetOwnDisplayName").Return(&mautrix.RespUserDisplayName{DisplayName: "Miounne"}, nil)
	suite.client.On("SetDisplayName", expected).Return(nil)
	suite.syncer.On("OnSync", mock.AnythingOfType("mautrix.SyncHandler"))
	suite.syncer.On("OnEventType", mock.AnythingOfType("event.Type"), mock.AnythingOfType("mautrix.EventHandler"))
	suite.client.On("Sync").Return(nil)

	suite.bot.Start(expected)
}

func (suite *MatrixBotSuite) TestSetProfile_WhoamiError() {
	expected := suite.text
	suite.client.On("Whoami").Return(&mautrix.RespWhoami{UserID: "@test:example.com", DeviceID: id.DeviceID(suite.text)}, errors.New(suite.text))

	err := suite.bot.setProfile(expected)

	suite.Error(err)
	suite.Equal(errors.New("cannot get whoami: Test"), err)
}

func (suite *MatrixBotSuite) TestSetProfile_GetOwnDisplayNameError() {
	expected := suite.text
	suite.client.On("Whoami").Return(&mautrix.RespWhoami{UserID: "@test:example.com", DeviceID: id.DeviceID(suite.text)}, nil)
	suite.client.On("GetOwnDisplayName").Return(&mautrix.RespUserDisplayName{DisplayName: "Miounne"}, errors.New(suite.text))

	err := suite.bot.setProfile(expected)

	suite.Error(err)
	suite.Equal(errors.New("cannot get display name: Test"), err)
}

func (suite *MatrixBotSuite) TestSetProfile_SetDisplayNameError() {
	expected := suite.text
	suite.client.On("Whoami").Return(&mautrix.RespWhoami{UserID: "@test:example.com", DeviceID: id.DeviceID(suite.text)}, nil)
	suite.client.On("GetOwnDisplayName").Return(&mautrix.RespUserDisplayName{DisplayName: "Miounne"}, nil)
	suite.client.On("SetDisplayName", expected).Return(errors.New(suite.text))

	err := suite.bot.setProfile(expected)

	suite.Error(err)
	suite.Equal(errors.New("cannot set display name: Test"), err)
}

func (suite *MatrixBotSuite) TestStop() {
	suite.bot.ticker = time.NewTicker(10 * time.Minute)
	suite.client.On("SetPresence", event.PresenceOffline).Return(errors.New("ignored error"))
	suite.logger.On("Error", "cannot set presence to offline: %v", errors.New("ignored error"))
	suite.client.On("Logout").Return(nil, errors.New("ignored error"))
	suite.logger.On("Error", "cannot logout: %v", errors.New("ignored error"))

	suite.bot.Stop()
}

func (suite *MatrixBotSuite) TestHandleCommand() {
	help := `
General _available by default_:

* **ping** - simple ping/pong, consider it as Mother Miounne healthcheck
* **help** - you're reading it

Registration _only if [matrix-registration](https://github.com/ZerataX/matrix-registration) integration enabled_:

* **registration list** - list all invite tokens
* **registration create** [MAX_USAGE] [EXPIRATION_DATE]- create new invite token (params are optional), eg: "registration create 1 2021-12-31" (can be used only once, before 31 Jan 2021) or "registration create" (no restrictions)
* **registration disable** TOKEN - disable the TOKEN
* **registration status** TOKEN - get info about the TOKEN

BMC _only if [buymeacoffee.com](https://buymeacoffee.com) integration enabled_:

* **bmc members** [STATUS] - list of bmc subscribers/members, STATUS is one of: active, inactive, all (default)
* **bmc supporters** - list of bmc one-time supporters
* **bmc extras** - list of extras purchases
`

	tests := []struct {
		input    string
		expected string
		setup    func()
	}{
		{input: "nothing", expected: "", setup: func() {}},
		{input: "ping", expected: suite.userID.String() + ": Pong! (took 1min to arrive)", setup: func() {}},
		{input: "!ping test", expected: suite.userID.String() + ": Pong! (ping test took 1min to arrive)", setup: func() {}},
		{input: "help", expected: help, setup: func() {}},
		{input: "wrong command", expected: "", setup: func() {}},
		{input: "registration", expected: help, setup: func() {}},
		{
			input:    "registration anything",
			expected: help,
			setup: func() {
				suite.registration.On("GetRoomID").Return(suite.roomID).Once()
			},
		},
		{
			input:    "registration help",
			expected: "That's not the admin room, I'm going to pretend that nothing was sent.",
			setup: func() {
				suite.registration.On("GetRoomID").Return(id.RoomID("!anotherroom@example.com")).Once()
			},
		},
		{
			input:    "registration list",
			expected: "Token **Test**\n\n* active: yes\n* expires: 2038-01-01\n* max usage: 1\n\n\n",
			setup: func() {
				suite.registration.On("GetRoomID").Return(suite.roomID).Once()
				suite.registration.On("GetList").Return([]models.RegistrationToken{
					{
						Name:     suite.text,
						Active:   true,
						Expires:  "2038-01-01",
						MaxUsage: 1,
						Used:     0,
					},
				}, nil).Once()
			},
		},
		{
			input:    "registration create 1 2038-01-01",
			expected: "New token: **Test**",
			setup: func() {
				suite.registration.On("GetRoomID").Return(suite.roomID).Once()
				suite.registration.On("CreateToken", "1", "2038-01-01").Return(&models.RegistrationToken{Name: suite.text}, nil)
			},
		},
		{
			input:    "registration disable Test",
			expected: "Token **Test** has been disabled",
			setup: func() {
				suite.registration.On("GetRoomID").Return(suite.roomID).Once()
				suite.registration.On("DisableToken", suite.text).Return(&models.RegistrationToken{Name: suite.text}, nil)
			},
		},
		{
			input:    "registration status Test",
			expected: "Token **Test**\n\n* active: yes\n* expires: 2038-01-01\n* max usage: 1\n\n\n",
			setup: func() {
				suite.registration.On("GetRoomID").Return(suite.roomID).Once()
				suite.registration.On("GetStatus", suite.text).Return(&models.RegistrationToken{
					Name:     suite.text,
					Active:   true,
					Expires:  "2038-01-01",
					MaxUsage: 1,
					Used:     0,
				}, nil)
			},
		},
		{input: "bmc", expected: help, setup: func() {}},
		{
			input:    "bmc anything",
			expected: help,
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
			},
		},
		{
			input:    "bmc help",
			expected: "That's not the admin room, I'm going to pretend that nothing was sent.",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(id.RoomID("!anotherroom@example.com")).Once()
			},
		},
		{
			input:    "bmc members active",
			expected: "**Error**: test",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
				suite.bmc.On("GetMembers", "active").Return(nil, errors.New("test")).Once()
			},
		},
		{
			input:    "bmc supporters",
			expected: "**Error**: test",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
				suite.bmc.On("GetSupporters").Return(nil, errors.New("test")).Once()
			},
		},
		{
			input:    "bmc extras",
			expected: "**Error**: test",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
				suite.bmc.On("GetExtras").Return(nil, errors.New("test")).Once()
			},
		},
		{
			input:    "bmc members",
			expected: "**Test** <test@example.com> (inactive)\n\n* joined: 2021-01-01 18:25:30\n* canceled: 2021-02-02 18:25:26\n* price: 10/month (USD)\n* message: hello\n\n\n",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
				suite.bmc.On("GetMembers", "all").Return([]*models.BMCMember{
					{
						CanceledAt: "2021-02-02 18:25:26",
						CreatedAt:  "2021-01-01 18:25:30",
						Currency:   "USD",
						Duration:   "month",
						Email:      "test@example.com",
						IsCanceled: true,
						Message:    "hello",
						Name:       suite.text,
						Price:      10,
						Quantity:   1,
					},
				}, nil).Once()
			},
		},
		{
			input:    "bmc supporters",
			expected: "**Test** <test@example.com> (inactive)\n\n* supported: 2021-01-01 18:25:30\n* price: 10 USD\n* message: hello\n\n\n",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
				suite.bmc.On("GetSupporters").Return([]*models.BMCSupporter{
					{
						CreatedAt:  "2021-01-01 18:25:30",
						Currency:   "USD",
						Email:      "test@example.com",
						IsRefunded: true,
						Message:    "hello",
						Name:       suite.text,
						Price:      10,
						Quantity:   1,
					},
				}, nil).Once()
			},
		},
		{
			input:    "bmc extras",
			expected: "**Test** <test@example.com> (revoked)\n\n* item: Test\n* paid: 2021-01-01 18:25:30\n* price: 10 USD\n* message: hello\n\n\n",
			setup: func() {
				suite.bmc.On("GetRoomID").Return(suite.roomID).Once()
				suite.bmc.On("GetExtras").Return([]*models.BMCPurchase{
					{
						CreatedAt: "2021-01-01 18:25:30",
						Currency:  "USD",
						Email:     "test@example.com",
						IsRevoked: true,
						Message:   "hello",
						Name:      suite.text,
						Price:     10,
						Title:     suite.text,
					},
				}, nil).Once()
			},
		},
	}
	for _, test := range tests {
		suite.Run(test.input, func() {
			test.setup()
			suite.client.On("UserTyping", suite.roomID, true, typingTimeout).Return(nil, nil)
			suite.client.On("UserTyping", suite.roomID, false, typingTimeout).Return(nil, nil)

			evt := &event.Event{
				Sender:    suite.userID,
				Timestamp: time.Now().UnixMilli() - 60100,
				Content: event.Content{
					Parsed: &event.MessageEventContent{
						Body: test.input,
					},
				},
			}
			actual := suite.bot.handleCommand(suite.roomID, evt)

			suite.Equal(test.expected, actual)
		})
	}
}

func (suite *MatrixBotSuite) TestTick() {
	suite.bot.ticker = time.NewTicker(100 * time.Millisecond)
	suite.logger.On("Debug", "checking active bmc members to send notification").Return()
	suite.logger.On("Debug", "checking inactive bmc members to send notification").Return()
	suite.logger.On("Debug", "checking bmc purchases to send notification").Return()
	suite.logger.On("Debug", "checking bmc supporters to send notification").Return()
	suite.logger.On("Debug", "checking registration tokens to send notification").Return()
	suite.bmc.On("GetNotifyExtras").Return(false)
	suite.bmc.On("GetNotifyMembers").Return(false)
	suite.bmc.On("GetNotifyPurchases").Return(false)
	suite.bmc.On("GetNotifySupporters").Return(false)
	suite.registration.On("GetNotifyNew").Return(false)
	suite.registration.On("GetNotifyUse").Return(false)

	go suite.bot.tick()
	time.Sleep(500 * time.Millisecond)
}

func TestMatrixBotSuite(t *testing.T) {
	suite.Run(t, new(MatrixBotSuite))
}
