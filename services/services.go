// Package services represents used services
package services

import "log"

// Logger interface
type Logger interface {
	GetLog() *log.Logger
	Error(message string, args ...interface{})
	Warn(message string, args ...interface{})
	Info(message string, args ...interface{})
	Debug(message string, args ...interface{})
	Trace(message string, args ...interface{})
}

// Policy is escape/sanitization policy for text
type Policy interface {
	Sanitize(string) string
}
