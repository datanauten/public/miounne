package services

import (
	"strconv"

	"git.sr.ht/~xn/bmc"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/models"
)

// BMCAPIclient is an actual buymeacoffee API client, interface used to simplify testing
type BMCAPIclient interface {
	GetMembers(string) (bmc.Members, error)
	GetSupporters() (bmc.Supporters, error)
	GetExtras() (bmc.Extras, error)
}

// BMC is a wrapper of BMCAPIclient with type conversions and internal ehancements
type BMC interface {
	GetRoomID() id.RoomID
	GetNotifyExtras() bool
	GetNotifyMembers() bool
	GetNotifySupporters() bool

	GetMembers(string) ([]*models.BMCMember, error)
	GetSupporters() ([]*models.BMCSupporter, error)
	GetExtras() ([]*models.BMCPurchase, error)
}

// bmc client wrapper
type bmclient struct {
	policy           Policy
	roomID           id.RoomID
	client           BMCAPIclient
	notifyExtras     bool
	notifyMembers    bool
	notifySupporters bool
}

// NewBMCClient creates a new Buymeacoffee API client
func NewBMCClient(bmcAPIclient BMCAPIclient, roomID id.RoomID, policy Policy, notifyExtras bool, notifyMembers bool, notifySupporters bool) BMC {
	return &bmclient{
		client:           bmcAPIclient,
		roomID:           roomID,
		policy:           policy,
		notifyExtras:     notifyExtras,
		notifyMembers:    notifyMembers,
		notifySupporters: notifySupporters,
	}
}

// GetRoomID returns admin room for bmc integration
func (b *bmclient) GetRoomID() id.RoomID {
	return b.roomID
}

// GetNotifyExtras config
func (b *bmclient) GetNotifyExtras() bool {
	return b.notifyExtras
}

// GetNotifyMembers config
func (b *bmclient) GetNotifyMembers() bool {
	return b.notifyMembers
}

// GetNotifySupporters config
func (b *bmclient) GetNotifySupporters() bool {
	return b.notifySupporters
}

// GetMembers returns buymeacoffee members list
func (b *bmclient) GetMembers(status string) ([]*models.BMCMember, error) {
	data, err := b.client.GetMembers(status)
	if err != nil {
		return nil, err
	}

	members := make([]*models.BMCMember, 0, len(data))
	for _, item := range data {
		members = append(members, &models.BMCMember{
			ID:         item.SubscriptionID,
			LevelID:    item.MembershipLevelID,
			Duration:   b.policy.Sanitize(item.SubscriptionDurationType),
			Price:      b.convertPrice(item.SubscriptionCoffeePrice),
			Quantity:   item.SubscriptionCoffeeNum,
			Currency:   b.policy.Sanitize(item.SubscriptionCurrency),
			Name:       b.policy.Sanitize(item.PayerName),
			Email:      b.policy.Sanitize(item.PayerEmail),
			Message:    b.policy.Sanitize(item.SubscriptionMessage),
			IsCanceled: item.SubscriptionIsCanceled > 0,
			CreatedAt:  b.policy.Sanitize(item.SubscriptionCreatedOn),
			UpdatedAt:  b.policy.Sanitize(item.SubscriptionUpdatedOn),
			CanceledAt: b.policy.Sanitize(item.SubscriptionCanceledOn),
		})
	}

	return members, nil
}

// GetSupporters returns buymeacoffee one-time supporters list
func (b *bmclient) GetSupporters() ([]*models.BMCSupporter, error) {
	data, err := b.client.GetSupporters()
	if err != nil {
		return nil, err
	}

	supporters := make([]*models.BMCSupporter, 0, len(data))
	for _, item := range data {
		supporters = append(supporters, &models.BMCSupporter{
			ID:         item.SupportID,
			Price:      b.convertPrice(item.SupportCoffeePrice),
			Quantity:   item.SupportCoffees,
			Currency:   b.policy.Sanitize(item.SupportCurrency),
			Name:       b.getSupporterName(item),
			Email:      b.getSupporterEmail(item),
			Message:    b.policy.Sanitize(item.SupportNote),
			IsRefunded: item.IsRefunded > 0,
			CreatedAt:  b.policy.Sanitize(item.SupportCreatedOn),
			UpdatedAt:  b.policy.Sanitize(item.SupportUpdatedOn),
		})
	}

	return supporters, nil
}

// GetExtras reeturns buymeacoffee extras purchases list
func (b *bmclient) GetExtras() ([]*models.BMCPurchase, error) {
	data, err := b.client.GetExtras()
	if err != nil {
		return nil, err
	}

	purchases := make([]*models.BMCPurchase, 0, len(data))
	for _, item := range data {
		purchases = append(purchases, &models.BMCPurchase{
			ID:        item.PurchaseID,
			Title:     b.policy.Sanitize(item.Extra.RewardTitle),
			Price:     b.convertPrice(item.PurchaseAmount),
			Currency:  b.policy.Sanitize(item.PurchaseCurrency),
			Name:      b.policy.Sanitize(item.PayerName),
			Email:     b.policy.Sanitize(item.PayerEmail),
			Message:   b.policy.Sanitize(item.PurchaseQuestion),
			IsRevoked: item.PurchaseIsRevoked > 0,
			CreatedAt: b.policy.Sanitize(item.PurchasedOn),
			UpdatedAt: b.policy.Sanitize(item.PurchaseUpdatedOn),
		})
	}

	return purchases, nil
}

func (b *bmclient) getSupporterName(supporter *bmc.Supporter) string {
	name := supporter.PayerName
	if supporter.SupporterName != "" {
		name = supporter.SupporterName
	}

	return b.policy.Sanitize(name)
}

func (b *bmclient) getSupporterEmail(supporter *bmc.Supporter) string {
	email := supporter.PayerEmail
	if supporter.SupportEmail != "" {
		email = supporter.SupportEmail
	}

	return b.policy.Sanitize(email)
}

func (b *bmclient) convertPrice(priceString string) float64 {
	price, err := strconv.ParseFloat(b.policy.Sanitize(priceString), 64)
	if err != nil {
		price = 0
	}

	return price
}
