package services

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

type server struct {
	http http.Server
	ipm  IPMatcher
	log  Logger
	rls  map[string]RateLimiter
	fh   FormHandler
}

// HTTPServer ...
type HTTPServer interface {
	Start()
}

// IPMatcher tries to match client IP from request headers and returns it as string
type IPMatcher interface {
	Match(*http.Request) string
	MatchHash(*http.Request) uint32
}

// NewHTTPServer creates new HTTP server
func NewHTTPServer(port string, logger Logger, ipMatcher IPMatcher, formHandler FormHandler, rls map[string]RateLimiter) HTTPServer {
	httpServer := &server{
		http: http.Server{
			Addr:     ":" + port,
			ErrorLog: logger.GetLog(),
		},
		log: logger,
		ipm: ipMatcher,
		rls: rls,
		fh:  formHandler,
	}

	httpServer.enableHealthcheck()
	httpServer.enableForms()

	return httpServer
}

// Start the server
func (s *server) Start() {
	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := s.http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(fmt.Errorf("cannot start the server: %v", err))
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	s.log.Info("shutting down the server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := s.http.Shutdown(ctx); err != nil {
		panic(fmt.Errorf("force shutdown of the server: %v", err))
	}
	s.log.Info("the server has been stopped")
}

func (s *server) enableHealthcheck() {
	http.HandleFunc("/_health", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		if _, err := w.Write([]byte(`{"status":"ok"}`)); err != nil {
			s.log.Error("%s %s %s %v", r.Method, r.URL.String(), s.ipm.Match(r), err)
		}
	})
}

func (s *server) enableForms() {
	http.HandleFunc("/form/", func(w http.ResponseWriter, r *http.Request) {
		var err error
		var body string

		name := strings.Replace(r.URL.Path, "/form/", "", 1)
		method := strings.ToUpper(r.Method)

		ip := s.ipm.Match(r)
		id := s.ipm.MatchHash(r)

		switch method {
		case http.MethodGet:
			body, err = s.fh.HandleGET(name, r)
		case http.MethodPost:
			if s.isLimited(id, name, method, r.URL.String(), w) {
				return
			}

			body, err = s.fh.HandlePOST(name, r)
		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			s.log.Warn("%s %s %s not allowed", method, r.URL.String(), ip)
		}

		if err != nil {
			s.log.Error("%s %s %s %v", method, r.URL.String(), ip, err)
			http.Error(w, err.Error(), 500)
			return
		}

		s.log.Info("%s %s %s", method, r.URL.String(), ip)

		if _, err := w.Write([]byte(body)); err != nil {
			s.log.Error("%s %s %s %v", r.Method, r.URL.String(), ip, err)
		}
	})
}

// isLimited checks if request should be rate limited
func (s *server) isLimited(id uint32, name, method, endpoint string, w http.ResponseWriter) bool {
	rl := s.rls[name]
	if rl == nil {
		return false
	}

	if rl.Allow(id) {
		return false
	}
	s.log.Info("%s %s by ID=%d has been ratelimited", method, endpoint, id)
	http.Error(w, "Too many requests", http.StatusTooManyRequests)

	return true
}
