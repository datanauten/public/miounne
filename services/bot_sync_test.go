package services

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/mocks"
)

type BotSyncSuite struct {
	suite.Suite
	roomID       id.RoomID
	userID       id.UserID
	text         string
	client       *mocks.MatrixClient
	syncer       *mocks.MatrixSyncer
	logger       *mocks.Logger
	registration *mocks.MatrixRegistrationClient
	bmc          *mocks.BMC
	bot          *bot
}

func (suite *BotSyncSuite) SetupTest() {
	suite.T().Helper()

	suite.roomID = id.RoomID("!test:example.com")
	suite.userID = id.UserID("@test:example.com")
	suite.text = "Test"
	suite.syncer = new(mocks.MatrixSyncer)
	suite.client = new(mocks.MatrixClient)
	suite.logger = new(mocks.Logger)
	suite.registration = new(mocks.MatrixRegistrationClient)
	suite.bmc = new(mocks.BMC)

	suite.bot = NewMatrixBot(
		suite.client,
		suite.syncer,
		suite.logger,
		10,
		suite.registration,
		suite.bmc,
	).(*bot)
	suite.bot.userID = suite.userID
}

func (suite *BotSyncSuite) TestSyncInviteEvent() {
	stateKey := suite.userID.String()
	evt := &event.Event{
		RoomID:   suite.roomID,
		StateKey: &stateKey,
		Content: event.Content{
			Parsed: &event.MemberEventContent{
				Membership:  event.MembershipInvite,
				AvatarURL:   id.ContentURIString("https://example.com"),
				Displayname: suite.text,
				IsDirect:    true,
				Reason:      suite.text,
				ThirdPartyInvite: &event.ThirdPartyInvite{
					DisplayName: suite.text,
				},
			},
		},
	}
	suite.logger.On("Debug", "sync, invite event = %t", true).Once()
	suite.client.On("JoinRoomByID", suite.roomID).Return(nil, nil)

	suite.bot.syncOnInviteEvent(0, evt)
}

func (suite *BotSyncSuite) TestSyncOnMessage() {
	stateKey := suite.userID.String()
	evt := &event.Event{
		StateKey:  &stateKey,
		RoomID:    suite.roomID,
		Sender:    suite.userID,
		Timestamp: time.Now().UnixMilli() + 1100,
		Content: event.Content{
			Parsed: &event.MessageEventContent{
				MsgType: event.MsgText,
				Body:    "ping",
			},
		},
	}
	resp := &event.MessageEventContent{MsgType: "m.text", Body: suite.userID.String() + ": Pong! (took 1.1sec to arrive)"}
	suite.client.On("SendMessageEvent", suite.roomID, evt, evt.Content.AsMessage()).Return(nil, nil).Once()
	suite.logger.On("Debug", "bot: new message in %s from %s: %s", suite.roomID, suite.userID, "ping").Once()
	suite.logger.On("Trace", "bot: response: %s", suite.userID.String()+": Pong! (took 1.1sec to arrive)").Once()
	suite.client.On("SendMessageEvent", suite.roomID, event.Type{Type: "m.room.message", Class: 1}, resp).Return(nil, nil).Once()

	suite.bot.syncOnMessage(0, evt)
}

func TestBotSyncSuite(t *testing.T) {
	suite.Run(t, new(BotSyncSuite))
}
