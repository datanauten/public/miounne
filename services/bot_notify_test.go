package services

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/mocks"
	"gitlab.com/etke.cc/miounne/v2/models"
)

type BotNotifySuite struct {
	suite.Suite
	roomID       id.RoomID
	userID       id.UserID
	text         string
	client       *mocks.MatrixClient
	syncer       *mocks.MatrixSyncer
	logger       *mocks.Logger
	registration *mocks.MatrixRegistrationClient
	bmc          *mocks.BMC
	bot          *bot
}

func (suite *BotNotifySuite) SetupTest() {
	suite.T().Helper()

	suite.roomID = id.RoomID("!test:example.com")
	suite.userID = id.UserID("@test:example.com")
	suite.text = "Test"
	suite.syncer = new(mocks.MatrixSyncer)
	suite.client = new(mocks.MatrixClient)
	suite.logger = new(mocks.Logger)
	suite.registration = new(mocks.MatrixRegistrationClient)
	suite.bmc = new(mocks.BMC)

	suite.bot = NewMatrixBot(
		suite.client,
		suite.syncer,
		suite.logger,
		10,
		suite.registration,
		suite.bmc,
	).(*bot)
	suite.bot.userID = suite.userID
}

func (suite *BotNotifySuite) TestNotifyBMCmembersInactive() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **inactive** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>inactive</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking inactive bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".inactive", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "inactive").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".inactive", accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCmembersInactive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersInactive_ErrorGetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **inactive** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>inactive</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking inactive bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".inactive", &accountDataIDs{}).Return(errors.New("T_TEST"))
	suite.logger.On("Warn", "cannot get %s BMC members from account data: %v", "inactive", errors.New("T_TEST")).Return()
	suite.bmc.On("GetMembers", "inactive").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".inactive", accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCmembersInactive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersInactive_ErrorGetMembers() {
	suite.logger.On("Debug", "checking inactive bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".inactive", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "inactive").Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot get %s BMC members from API: %v", "inactive", errors.New("T_TEST")).Return()
	suite.logger.On("Debug", "no new inactive bmc members").Return()

	suite.bot.notifyBMCmembersInactive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersInactive_ErrorSetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **inactive** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>inactive</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking inactive bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".inactive", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "inactive").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".inactive", accountDataIDs{IDs: []int{1}}).Return(errors.New("T_TEST"))
	suite.logger.On("Error", "cannot set %s BMC members to account data: %v", "inactive", errors.New("T_TEST")).Return()
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCmembersInactive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersInactive_ErrorSendMessage() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **inactive** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>inactive</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking inactive bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".inactive", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "inactive").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".inactive", accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot send bmc inactive members notification: %v", errors.New("T_TEST")).Return()

	suite.bot.notifyBMCmembersInactive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersActive() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **active** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>active</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking active bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".active", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "active").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".active", accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCmembersActive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersActive_ErrorGetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **active** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>active</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking active bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".active", &accountDataIDs{}).Return(errors.New("T_TEST"))
	suite.logger.On("Warn", "cannot get %s BMC members from account data: %v", "active", errors.New("T_TEST")).Return()
	suite.bmc.On("GetMembers", "active").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".active", accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCmembersActive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersActive_ErrorGetMembers() {
	suite.logger.On("Debug", "checking active bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".active", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "active").Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot get %s BMC members from API: %v", "active", errors.New("T_TEST")).Return()
	suite.logger.On("Debug", "no new active bmc members").Return()

	suite.bot.notifyBMCmembersActive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersActive_ErrorSetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **active** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>active</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking active bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".active", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "active").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".active", accountDataIDs{IDs: []int{1}}).Return(errors.New("T_TEST"))
	suite.logger.On("Error", "cannot set %s BMC members to account data: %v", "active", errors.New("T_TEST")).Return()
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCmembersActive()
}

func (suite *BotNotifySuite) TestNotifyBMCmembersActive_ErrorSendMessage() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **active** member\n\n**test** test@example.com (mailto:test@example.com)\n\n* joined: 2020-07-29 15:15:22\n* price: 2.99/month (USD)",
		FormattedBody: "<p>new <strong>active</strong> member</p>\n\n<p><strong>test</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>joined: 2020-07-29 15:15:22<br />\n</li>\n<li>price: 2.99/month (USD)<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking active bmc members to send notification").Return()
	suite.bmc.On("GetNotifyMembers").Return(true)
	suite.client.On("GetAccountData", eventBMCmembers+".active", &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetMembers", "active").Return([]*models.BMCMember{{
		ID:         1,
		LevelID:    5,
		Duration:   "month",
		Price:      2.99,
		Quantity:   1,
		Currency:   "USD",
		Name:       "test",
		Email:      "test@example.com",
		IsCanceled: false,
		CreatedAt:  "2020-07-29 15:15:22",
		UpdatedAt:  "2020-07-29 15:15:22",
		CanceledAt: "",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCmembers+".active", accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot send bmc active members notification: %v", errors.New("T_TEST")).Return()

	suite.bot.notifyBMCmembersActive()
}

func (suite *BotNotifySuite) TestNotifyBMCsupporters() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **supporter**\n\n**Test User** test@example.com (mailto:test@example.com)\n\n* supported: 2020-08-05 09:34:43\n* price: 17.7 GBP\n* message: Heya!",
		FormattedBody: "<p>new <strong>supporter</strong></p>\n\n<p><strong>Test User</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>supported: 2020-08-05 09:34:43<br />\n</li>\n<li>price: 17.7 GBP<br />\n</li>\n<li>message: Heya!<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc supporters to send notification").Return()
	suite.bmc.On("GetNotifySupporters").Return(true)
	suite.client.On("GetAccountData", eventBMCsupporters, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetSupporters").Return([]*models.BMCSupporter{{
		ID:         1,
		Price:      3.54,
		Quantity:   5,
		Currency:   "GBP",
		Name:       "Test User",
		Email:      "test@example.com",
		Message:    "Heya!",
		IsRefunded: false,
		CreatedAt:  "2020-08-05 09:34:43",
		UpdatedAt:  "2020-08-05 09:34:43",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCsupporters, accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCsupporters()
}

func (suite *BotNotifySuite) TestNotifyBMCsupporters_ErrorGetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **supporter**\n\n**Test User** test@example.com (mailto:test@example.com)\n\n* supported: 2020-08-05 09:34:43\n* price: 17.7 GBP\n* message: Heya!",
		FormattedBody: "<p>new <strong>supporter</strong></p>\n\n<p><strong>Test User</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>supported: 2020-08-05 09:34:43<br />\n</li>\n<li>price: 17.7 GBP<br />\n</li>\n<li>message: Heya!<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc supporters to send notification").Return()
	suite.bmc.On("GetNotifySupporters").Return(true)
	suite.client.On("GetAccountData", eventBMCsupporters, &accountDataIDs{}).Return(errors.New("T_TEST"))
	suite.logger.On("Warn", "cannot get BMC supporters from account data: %v", errors.New("T_TEST")).Return()
	suite.bmc.On("GetSupporters").Return([]*models.BMCSupporter{{
		ID:         1,
		Price:      3.54,
		Quantity:   5,
		Currency:   "GBP",
		Name:       "Test User",
		Email:      "test@example.com",
		Message:    "Heya!",
		IsRefunded: false,
		CreatedAt:  "2020-08-05 09:34:43",
		UpdatedAt:  "2020-08-05 09:34:43",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCsupporters, accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCsupporters()
}

func (suite *BotNotifySuite) TestNotifyBMCsupporters_ErrorGetSupporters() {
	suite.logger.On("Debug", "checking bmc supporters to send notification").Return()
	suite.bmc.On("GetNotifySupporters").Return(true)
	suite.client.On("GetAccountData", eventBMCsupporters, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetSupporters").Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot get BMC supporters from API: %v", errors.New("T_TEST")).Return()
	suite.logger.On("Debug", "no new bmc supporters").Return()

	suite.bot.notifyBMCsupporters()
}

func (suite *BotNotifySuite) TestNotifyBMCsupporters_ErrorSetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **supporter**\n\n**Test User** test@example.com (mailto:test@example.com)\n\n* supported: 2020-08-05 09:34:43\n* price: 17.7 GBP\n* message: Heya!",
		FormattedBody: "<p>new <strong>supporter</strong></p>\n\n<p><strong>Test User</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>supported: 2020-08-05 09:34:43<br />\n</li>\n<li>price: 17.7 GBP<br />\n</li>\n<li>message: Heya!<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc supporters to send notification").Return()
	suite.bmc.On("GetNotifySupporters").Return(true)
	suite.client.On("GetAccountData", eventBMCsupporters, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetSupporters").Return([]*models.BMCSupporter{{
		ID:         1,
		Price:      3.54,
		Quantity:   5,
		Currency:   "GBP",
		Name:       "Test User",
		Email:      "test@example.com",
		Message:    "Heya!",
		IsRefunded: false,
		CreatedAt:  "2020-08-05 09:34:43",
		UpdatedAt:  "2020-08-05 09:34:43",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCsupporters, accountDataIDs{IDs: []int{1}}).Return(errors.New("T_TEST"))
	suite.logger.On("Error", "cannot set BMC supporters to account data: %v", errors.New("T_TEST")).Return()
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCsupporters()
}

func (suite *BotNotifySuite) TestNotifyBMCsupporters_ErrorSendMessage() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **supporter**\n\n**Test User** test@example.com (mailto:test@example.com)\n\n* supported: 2020-08-05 09:34:43\n* price: 17.7 GBP\n* message: Heya!",
		FormattedBody: "<p>new <strong>supporter</strong></p>\n\n<p><strong>Test User</strong> <a href=\"mailto:test@example.com\">test@example.com</a></p>\n\n<ul>\n<li>supported: 2020-08-05 09:34:43<br />\n</li>\n<li>price: 17.7 GBP<br />\n</li>\n<li>message: Heya!<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc supporters to send notification").Return()
	suite.bmc.On("GetNotifySupporters").Return(true)
	suite.client.On("GetAccountData", eventBMCsupporters, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetSupporters").Return([]*models.BMCSupporter{{
		ID:         1,
		Price:      3.54,
		Quantity:   5,
		Currency:   "GBP",
		Name:       "Test User",
		Email:      "test@example.com",
		Message:    "Heya!",
		IsRefunded: false,
		CreatedAt:  "2020-08-05 09:34:43",
		UpdatedAt:  "2020-08-05 09:34:43",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCsupporters, accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot send bmc supporters notification: %v", errors.New("T_TEST")).Return()

	suite.bot.notifyBMCsupporters()
}

func (suite *BotNotifySuite) TestNotifyBMCpurchases() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **purchase**\n\n**** example@example.com (mailto:example@example.com)\n\n* item: AMA\n* paid: 2020-05-20 08:27:34\n* price: 1.54 USD\n* message: share your email?",
		FormattedBody: "<p>new <strong>purchase</strong></p>\n\n<p>**** <a href=\"mailto:example@example.com\">example@example.com</a></p>\n\n<ul>\n<li>item: AMA<br />\n</li>\n<li>paid: 2020-05-20 08:27:34<br />\n</li>\n<li>price: 1.54 USD<br />\n</li>\n<li>message: share your email?<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc purchases to send notification").Return()
	suite.bmc.On("GetNotifyExtras").Return(true)
	suite.client.On("GetAccountData", eventBMCpurchases, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetExtras").Return([]*models.BMCPurchase{{
		ID:        1,
		Title:     "AMA",
		Price:     1.54,
		Currency:  "USD",
		Name:      "",
		Email:     "example@example.com",
		Message:   "share your email?",
		IsRevoked: false,
		CreatedAt: "2020-05-20 08:27:34",
		UpdatedAt: "2020-05-20 08:27:34",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCpurchases, accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCpurchases()
}

func (suite *BotNotifySuite) TestNotifyBMCpurchases_ErrorGetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **purchase**\n\n**** example@example.com (mailto:example@example.com)\n\n* item: AMA\n* paid: 2020-05-20 08:27:34\n* price: 1.54 USD\n* message: share your email?",
		FormattedBody: "<p>new <strong>purchase</strong></p>\n\n<p>**** <a href=\"mailto:example@example.com\">example@example.com</a></p>\n\n<ul>\n<li>item: AMA<br />\n</li>\n<li>paid: 2020-05-20 08:27:34<br />\n</li>\n<li>price: 1.54 USD<br />\n</li>\n<li>message: share your email?<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc purchases to send notification").Return()
	suite.bmc.On("GetNotifyExtras").Return(true)
	suite.client.On("GetAccountData", eventBMCpurchases, &accountDataIDs{}).Return(errors.New("T_TEST"))
	suite.logger.On("Warn", "cannot get BMC purchases from account data: %v", errors.New("T_TEST")).Return()
	suite.bmc.On("GetExtras").Return([]*models.BMCPurchase{{
		ID:        1,
		Title:     "AMA",
		Price:     1.54,
		Currency:  "USD",
		Name:      "",
		Email:     "example@example.com",
		Message:   "share your email?",
		IsRevoked: false,
		CreatedAt: "2020-05-20 08:27:34",
		UpdatedAt: "2020-05-20 08:27:34",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCpurchases, accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCpurchases()
}

func (suite *BotNotifySuite) TestNotifyBMCpurchases_ErrorGetExtras() {
	suite.logger.On("Debug", "checking bmc purchases to send notification").Return()
	suite.bmc.On("GetNotifyExtras").Return(true)
	suite.client.On("GetAccountData", eventBMCpurchases, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetExtras").Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot get BMC purchases from API: %v", errors.New("T_TEST")).Return()
	suite.logger.On("Debug", "no new bmc purchases").Return()

	suite.bot.notifyBMCpurchases()
}

func (suite *BotNotifySuite) TestNotifyBMCpurchases_ErrorSetAccountData() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **purchase**\n\n**** example@example.com (mailto:example@example.com)\n\n* item: AMA\n* paid: 2020-05-20 08:27:34\n* price: 1.54 USD\n* message: share your email?",
		FormattedBody: "<p>new <strong>purchase</strong></p>\n\n<p>**** <a href=\"mailto:example@example.com\">example@example.com</a></p>\n\n<ul>\n<li>item: AMA<br />\n</li>\n<li>paid: 2020-05-20 08:27:34<br />\n</li>\n<li>price: 1.54 USD<br />\n</li>\n<li>message: share your email?<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc purchases to send notification").Return()
	suite.bmc.On("GetNotifyExtras").Return(true)
	suite.client.On("GetAccountData", eventBMCpurchases, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetExtras").Return([]*models.BMCPurchase{{
		ID:        1,
		Title:     "AMA",
		Price:     1.54,
		Currency:  "USD",
		Name:      "",
		Email:     "example@example.com",
		Message:   "share your email?",
		IsRevoked: false,
		CreatedAt: "2020-05-20 08:27:34",
		UpdatedAt: "2020-05-20 08:27:34",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCpurchases, accountDataIDs{IDs: []int{1}}).Return(errors.New("T_TEST"))
	suite.logger.On("Error", "cannot set BMC purchases to account data: %v", errors.New("T_TEST")).Return()
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, nil)

	suite.bot.notifyBMCpurchases()
}

func (suite *BotNotifySuite) TestNotifyBMCpurchases_ErrorSendMessage() {
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContent := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "new **purchase**\n\n**** example@example.com (mailto:example@example.com)\n\n* item: AMA\n* paid: 2020-05-20 08:27:34\n* price: 1.54 USD\n* message: share your email?",
		FormattedBody: "<p>new <strong>purchase</strong></p>\n\n<p>**** <a href=\"mailto:example@example.com\">example@example.com</a></p>\n\n<ul>\n<li>item: AMA<br />\n</li>\n<li>paid: 2020-05-20 08:27:34<br />\n</li>\n<li>price: 1.54 USD<br />\n</li>\n<li>message: share your email?<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking bmc purchases to send notification").Return()
	suite.bmc.On("GetNotifyExtras").Return(true)
	suite.client.On("GetAccountData", eventBMCpurchases, &accountDataIDs{}).Return(nil)
	suite.bmc.On("GetExtras").Return([]*models.BMCPurchase{{
		ID:        1,
		Title:     "AMA",
		Price:     1.54,
		Currency:  "USD",
		Name:      "",
		Email:     "example@example.com",
		Message:   "share your email?",
		IsRevoked: false,
		CreatedAt: "2020-05-20 08:27:34",
		UpdatedAt: "2020-05-20 08:27:34",
	}}, nil)
	suite.client.On("SetAccountData", eventBMCpurchases, accountDataIDs{IDs: []int{1}}).Return(nil)
	suite.bmc.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContent).Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot send bmc purchases notification: %v", errors.New("T_TEST")).Return()

	suite.bot.notifyBMCpurchases()
}

func (suite *BotNotifySuite) TestNotifyRegistrationTokens() {
	null := accountDataMSI(nil)
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContentNew := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **created**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>created</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	evtContentUse := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **used**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>used</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking registration tokens to send notification").Return()
	suite.registration.On("GetNotifyNew").Return(true)
	suite.client.On("GetAccountData", eventRegisrationTokens, &null).Return(nil)
	suite.registration.On("GetList").Return([]models.RegistrationToken{{
		Name:     "test",
		Valid:    true,
		Disabled: false,
		MaxUsage: 2,
		Used:     1,
		Expires:  "",
	}}, nil)
	suite.client.On("SetAccountData", eventRegisrationTokens, accountDataMSI{"test": 1}).Return(nil)
	suite.registration.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentNew).Return(nil, nil).Once()
	suite.registration.On("GetNotifyUse").Return(true)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentUse).Return(nil, nil).Once()

	suite.bot.notifyRegistrationTokens()
}

func (suite *BotNotifySuite) TestNotifyRegistrationTokens_ErrorGetAccountData() {
	null := accountDataMSI(nil)
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContentNew := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **created**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>created</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	evtContentUse := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **used**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>used</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking registration tokens to send notification").Return()
	suite.registration.On("GetNotifyNew").Return(true)
	suite.client.On("GetAccountData", eventRegisrationTokens, &null).Return(errors.New("T_TEST"))
	suite.logger.On("Warn", "cannot get registration tokens from account data: %v", errors.New("T_TEST"))
	suite.registration.On("GetList").Return([]models.RegistrationToken{{
		Name:     "test",
		Valid:    true,
		Disabled: false,
		MaxUsage: 2,
		Used:     1,
		Expires:  "",
	}}, nil)
	suite.client.On("SetAccountData", eventRegisrationTokens, accountDataMSI{"test": 1}).Return(nil)
	suite.registration.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentNew).Return(nil, nil).Once()
	suite.registration.On("GetNotifyUse").Return(true)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentUse).Return(nil, nil).Once()

	suite.bot.notifyRegistrationTokens()
}

func (suite *BotNotifySuite) TestNotifyRegistrationTokens_ErrorSetAccountData() {
	null := accountDataMSI(nil)
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContentNew := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **created**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>created</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	evtContentUse := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **used**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>used</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking registration tokens to send notification").Return()
	suite.registration.On("GetNotifyNew").Return(true)
	suite.client.On("GetAccountData", eventRegisrationTokens, &null).Return(nil)
	suite.registration.On("GetList").Return([]models.RegistrationToken{{
		Name:     "test",
		Valid:    true,
		Disabled: false,
		MaxUsage: 2,
		Used:     1,
		Expires:  "",
	}}, nil)
	suite.client.On("SetAccountData", eventRegisrationTokens, accountDataMSI{"test": 1}).Return(errors.New("T_TEST"))
	suite.logger.On("Error", "cannot set registration tokens to account data: %v", errors.New("T_TEST")).Return()
	suite.registration.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentNew).Return(nil, nil).Once()
	suite.registration.On("GetNotifyUse").Return(true)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentUse).Return(nil, nil).Once()

	suite.bot.notifyRegistrationTokens()
}

func (suite *BotNotifySuite) TestNotifyRegistrationTokens_ErrorGetList() {
	null := accountDataMSI(nil)
	suite.logger.On("Debug", "checking registration tokens to send notification").Return()
	suite.registration.On("GetNotifyNew").Return(true)
	suite.registration.On("GetNotifyUse").Return(true)
	suite.client.On("GetAccountData", eventRegisrationTokens, &null).Return(nil)
	suite.registration.On("GetList").Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot get registration tokens from API: %v", errors.New("T_TEST")).Return()

	suite.bot.notifyRegistrationTokens()
}

func (suite *BotNotifySuite) TestNotifyRegistrationTokens_ErrorSendMessage() {
	null := accountDataMSI(nil)
	evtType := event.Type{Type: "m.room.message", Class: 1}
	evtContentNew := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **created**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>created</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	evtContentUse := event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "token has been **used**\n\nToken **test**\n\n* active: yes\n* max usage: 2\n* already used: 1",
		FormattedBody: "<p>token has been <strong>used</strong></p>\n\n<p>Token <strong>test</strong></p>\n\n<ul>\n<li>active: yes<br />\n</li>\n<li>max usage: 2<br />\n</li>\n<li>already used: 1<br />\n</li>\n</ul>",
		Format:        "org.matrix.custom.html",
	}
	suite.logger.On("Debug", "checking registration tokens to send notification").Return()
	suite.registration.On("GetNotifyNew").Return(true)
	suite.client.On("GetAccountData", eventRegisrationTokens, &null).Return(nil)
	suite.registration.On("GetList").Return([]models.RegistrationToken{{
		Name:     "test",
		Valid:    true,
		Disabled: false,
		MaxUsage: 2,
		Used:     1,
		Expires:  "",
	}}, nil)
	suite.client.On("SetAccountData", eventRegisrationTokens, accountDataMSI{"test": 1}).Return(nil)
	suite.registration.On("GetRoomID").Return(suite.roomID)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentNew).Return(nil, errors.New("T_TEST"))
	suite.logger.On("Error", "cannot send registration tokens notification: %v", errors.New("T_TEST")).Return().Twice()
	suite.registration.On("GetNotifyUse").Return(true)
	suite.client.On("SendMessageEvent", suite.roomID, evtType, &evtContentUse).Return(nil, errors.New("T_TEST")).Once()

	suite.bot.notifyRegistrationTokens()
}

func TestBotNotifySuite(t *testing.T) {
	suite.Run(t, new(BotNotifySuite))
}
