package utils

import (
	"runtime"
	"testing"

	"github.com/stretchr/testify/suite"
)

type NetworkValidatorSuite struct {
	suite.Suite
	v *netValidator
}

func (suite *NetworkValidatorSuite) SetupTest() {
	suite.T().Helper()

	// copy of internal/testenv.MustHaveExternalNetwork()
	// because use of internal/ packages is prohibited,
	// but NetworkValidator tests require external network
	if runtime.GOOS == "js" {
		suite.T().Skipf("skipping test: no external network on %s", runtime.GOOS)
	}
	if testing.Short() {
		suite.T().Skipf("skipping test: no external network in -short mode")
	}

	suite.v = NewNetworkValidator("TRACE").(*netValidator)
}

func (suite *NetworkValidatorSuite) TestNewNetworkValidator() {
	validator := NewNetworkValidator("TRACE")

	suite.IsType(&netValidator{}, validator)
}

func (suite *NetworkValidatorSuite) TestHasA() {
	tests := map[string]bool{
		"example.com":   true,
		"doesnt.exists": false,
	}

	for host, expected := range tests {
		suite.Run(host, func() {
			result := suite.v.HasA(host)

			suite.Equal(expected, result)
		})
	}
}

func (suite *NetworkValidatorSuite) TestHasCNAME() {
	tests := map[string]bool{
		"example.com":   true,
		"doesnt.exists": false,
	}

	for host, expected := range tests {
		suite.Run(host, func() {
			result := suite.v.HasCNAME(host)

			suite.Equal(expected, result)
		})
	}
}

func (suite *NetworkValidatorSuite) TestHasMX() {
	tests := map[string]bool{
		"example.com":   true,
		"doesnt.exists": false,
	}

	for host, expected := range tests {
		suite.Run(host, func() {
			result := suite.v.HasMX(host)

			suite.Equal(expected, result)
		})
	}
}

func TestNetworkValidatorSuite(t *testing.T) {
	suite.Run(t, new(NetworkValidatorSuite))
}
