package utils

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
)

// constants are here to make linter happy!
const (
	txtERROR   = "ERROR"
	txtWARNING = "WARNING"
	txtINFO    = "INFO"
	txtDEBUG   = "DEBUG"
	txtTRACE   = "TRACE"
)

type LoggerSuite struct {
	suite.Suite
}

func (suite *LoggerSuite) SetupTest() {
	suite.T().Helper()
}

// catchStdout re-routes stdout to separate pipe before function and switches it back after that, returns text catched in separate pipe
func (suite *LoggerSuite) catchStdout(function func()) string {
	suite.T().Helper()

	osStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	function()

	w.Close()
	out, err := ioutil.ReadAll(r)
	suite.NoError(err)
	os.Stdout = osStdout

	return string(out)
}

func (suite *LoggerSuite) TestNewLogger() {
	log := NewLogger("test", "example")

	suite.True(log.level == INFO)
}

func (suite *LoggerSuite) TestGetLog() {
	logger := NewLogger("", txtINFO)
	stdLogger := log.New(os.Stdout, "", 0)

	suite.Equal(stdLogger, logger.GetLog())
}

func (suite *LoggerSuite) TestError() {
	expected := txtERROR + " Test\n"

	actual := suite.catchStdout(func() {
		logger := NewLogger("", txtERROR)
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	suite.Equal(expected, actual)
}

func (suite *LoggerSuite) TestWarn() {
	expected := txtERROR + " Test\n"
	expected += txtWARNING + " Test\n"

	actual := suite.catchStdout(func() {
		logger := NewLogger("", txtWARNING)
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	suite.Equal(expected, actual)
}

func (suite *LoggerSuite) TestInfo() {
	expected := txtERROR + " Test\n"
	expected += txtWARNING + " Test\n"
	expected += txtINFO + " Test\n"

	actual := suite.catchStdout(func() {
		logger := NewLogger("", txtINFO)
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	suite.Equal(expected, actual)
}

func (suite *LoggerSuite) TestDebug() {
	expected := txtERROR + " Test\n"
	expected += txtWARNING + " Test\n"
	expected += txtINFO + " Test\n"
	expected += txtDEBUG + " Test\n"

	actual := suite.catchStdout(func() {
		logger := NewLogger("", txtDEBUG)
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	suite.Equal(expected, actual)
}

func (suite *LoggerSuite) TestTrace() {
	expected := txtERROR + " Test\n"
	expected += txtWARNING + " Test\n"
	expected += txtINFO + " Test\n"
	expected += txtDEBUG + " Test\n"
	expected += txtTRACE + " Test\n"

	actual := suite.catchStdout(func() {
		logger := NewLogger("", txtTRACE)
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	suite.Equal(expected, actual)
}

func (suite *LoggerSuite) TestTrace_Skip() {
	expected := ""

	actual := suite.catchStdout(func() {
		logger := NewLogger("", txtTRACE)
		logger.Trace("Got membership state event")
	})

	suite.Equal(expected, actual)
}

func TestLoggerSuite(t *testing.T) {
	suite.Run(t, new(LoggerSuite))
}
