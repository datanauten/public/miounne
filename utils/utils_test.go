package utils

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type UtilsSuite struct {
	suite.Suite
}

func (suite *UtilsSuite) SetupTest() {
	suite.T().Helper()
}

func (suite *UtilsSuite) TestGetBaseDomain() {
	tests := []struct {
		input    string
		expected string
	}{
		{input: "example.com", expected: "example.com"},
		{input: "matrix.example.com", expected: "example.com"},
		{input: "notavaliddomain", expected: "notavaliddomain"},
	}

	for _, test := range tests {
		actual := GetBaseDomain(test.input)

		suite.Equal(test.expected, actual)
	}
}

func (suite *UtilsSuite) TestAbs() {
	tests := []struct {
		input    int64
		expected int64
	}{
		{input: -123, expected: 123},
		{input: 321, expected: 321},
	}

	for _, test := range tests {
		actual := Abs(test.input)

		suite.Equal(test.expected, actual)
	}
}

func TestUtilsSuite(t *testing.T) {
	suite.Run(t, new(UtilsSuite))
}
