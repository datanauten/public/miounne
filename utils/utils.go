package utils

import (
	"strings"
)

// GetBaseDomain from url
func GetBaseDomain(host string) string {
	parts := strings.Split(host, ".")
	size := len(parts)
	// If domain contains only 2 parts (or less) - consider it without subdomains
	if size <= 2 {
		return host
	}

	return parts[size-2] + "." + parts[size-1]
}

// Abs olute value
func Abs(value int64) int64 {
	if value < 0 {
		return -value
	}
	return value
}
