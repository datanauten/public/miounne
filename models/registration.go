package models

// RegistrationToken representation
type RegistrationToken struct {
	Name     string `json:"name"`
	Active   bool   `json:"active"`
	Valid    bool   `json:"valid,omitempty"` // for older versions
	Disabled bool   `json:"disabled"`
	Expires  string `json:"expiration_date"`
	ExDate   string `json:"ex_date"` // for older versions
	MaxUsage int    `json:"max_usage"`
	Once     bool   `json:"one_time,omitempty"` // for older versions
	Used     int    `json:"used"`
}

// Finalize response
func (token *RegistrationToken) Finalize() {
	// older version
	if token.Valid || token.Once {
		token.Active = token.Valid
		token.Expires = token.ExDate
		if token.Once {
			token.MaxUsage = 1
		}
	}
}
