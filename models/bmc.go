package models

// BMCMember is a buymeacoffee subscriber representation
type BMCMember struct {
	ID       int    `json:"id"`
	LevelID  int    `json:"level_id"`
	Duration string `json:"duration"`

	Price    float64 `json:"price"`
	Quantity int     `json:"quantity"`
	Currency string  `json:"currency"`

	Name    string `json:"name"`
	Email   string `json:"email"`
	Message string `json:"message"`

	IsCanceled bool   `json:"is_canceled"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
	CanceledAt string `json:"canceled_at"`
}

// BMCSupporter is a buymeacoffee one-time payer representation
type BMCSupporter struct {
	ID int `json:"id"`

	Price    float64 `json:"price"`
	Quantity int     `json:"quantity"`
	Currency string  `json:"currency"`

	Name    string `json:"name"`
	Email   string `json:"email"`
	Message string `json:"message"`

	IsRefunded bool   `json:"is_refunded"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

// BMCPurchase is a buymeacoffee extras purchase representation
type BMCPurchase struct {
	ID int `json:"id"`

	Title    string  `json:"title"`
	Price    float64 `json:"price"`
	Currency string  `json:"currency"`

	Name    string `json:"name"`
	Email   string `json:"email"`
	Message string `json:"message"`

	IsRevoked bool   `json:"is_revoked"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}
