package models

func (suite *ModelSuite) TestRegistrationToken_Finalize() {
	oldToken := &RegistrationToken{
		Valid:  true,
		Once:   true,
		ExDate: "1999-12-12",
	}
	newToken := &RegistrationToken{
		Active:   true,
		Expires:  "1999-12-12",
		MaxUsage: 1,
	}

	oldToken.Finalize()
	newToken.Finalize()

	suite.Equal(newToken.Active, oldToken.Active)
	suite.Equal(newToken.Expires, oldToken.Expires)
	suite.Equal(newToken.MaxUsage, newToken.MaxUsage)
}
