module gitlab.com/etke.cc/miounne/v2

go 1.17

require (
	git.sr.ht/~xn/bmc v1.0.1
	github.com/microcosm-cc/bluemonday v1.0.17
	github.com/sethvargo/go-retry v0.2.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
	maunium.net/go/mautrix v0.10.10
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/btcsuite/btcutil v1.0.2 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/objx v0.1.0 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
