# Miounne v2.2.2 (2022-01-10)

### Features :sparkles:

* Multi-arch docker image (arm32/v7, arm64/v8, amd64)

### Misc :zzz:

* Updated dependencies
* Vendored dependencies
* Fixed update target

# Miounne v2.2.1 (2021-12-13)

### Features :sparkles:

_small QoL things_

* Fast starts/restarts (saving next batch sync token in the matrix user's account data to start from exact same position where the last sync was done)
* Typing notifications (no, bot doesn't freeze, that's just a slow API call)


### Misc :zzz:

* Updated dependencies

# Miounne v2.2.0 (2021-11-16)

### Features :sparkles:

* [Buymeacoffee](https://buymeacoffee.com?via=etkecc) notifications about new subscribers, extras purchases and supporters
* [matrix-registration](https://github.com/ZerataX/matrix-registration) notifications about new and used tokens

### Misc :zzz:

* Updated dependencies

# Miounne v2.1.0 (2021-11-01)

### Features :sparkles:

* [Buymeacoffee](https://buymeacoffee.com?via=etkecc) integration!
* Rate limiter for http forms
* Enhanced email validation in http forms (MX lookups, length checks, regexp match)

### Misc :zzz:

* Updated dependencies
* Updated README to reflect required and optional config sections
* Auto-shrink long messages if they exceed matrix max allowed payload size (64kb) with note

# Miounne v2.0.0 (2021-10-19)

### Breaking changes :warning:

* Removed E2E support - we failed to make it more stable and it was unusable.
* Removed database dependency - database was used only for encryption stack, when it's gone no need for db

# Miounne v1.1.0 (2021-10-19)

### Features :sparkles:

* Form extensions (consider this one as "to-be" feature)
* `ping` (and `!ping`) command now measures time-to-arrive, mimics [maubot/echo](https://github.com/maubot/echo)
* IP logging can be enabled or disabled with `MIOUNNE_LOG_IP` var
* [Now released for multiple OSes and architectures](https://gitlab.com/etke.cc/miounne/-/releases)

### Bugfixes :bug:

* Returned missing HTTP logs
* Fix motd types
* Return human-readable error if matrix-registration backend not found (config set, but returns 404)
* Fix auto-join rooms without persistent storage

### Misc :zzz:

* Refactored MatrixSQL, MatrixRegistration, MatrixBot, MatrixRegistrationToken, FormHandler
* Added trimming of whitespaces (like double-space or tripple-space) in config env values and form fields values
* Changed log levels in http shutdown flows
* Added version to motd (run from source = development, build main branch = latest, build git tag = vx.x.x)
* Updated dependencies
* Updated linter config
* Cleanup dockerfile
* Switched build mode to [PIE](https://en.wikipedia.org/wiki/Position-independent_code) in docker images

# Miounne v1.0.2 (2021-09-12)

### Features :sparkles:

* Send encrypted message when room is encrypted, even if plaintext mode selected (`bot.SendMessage` will check room encryption status and call `bot.SendEncryptedMessage` instead)

### Bugfixes :bug:

* Properly handle form without email

### Misc :zzz:

* Updated README

# Miounne v1.0.1 (2021-09-09)

### Misc :zzz:

* Updated dependencies
* Refactored matrix-registration integration

# Miounne v1.0.0 (2021-09-04)

### Features :sparkles:

* HTTP forms handling
* matrix-registration integration
* Antispam
* Persistent store (crypto & state)
* Matrix bot
* E2E support (alpha-grade)
* Tagged [releases](https://gitlab.com/etke.cc/miounne/-/releases) and [docker images](https://gitlab.com/etke.cc/miounne/container_registry)

### Bugfixes :bug:

N/A _that's the first version, bugs are creating on that step!_

### Breaking changes :warning:

Mother Miounne has been created.
