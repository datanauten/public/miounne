package bmc

import (
	"fmt"
)

// Member is a BMC subscriber
type Member struct {
	Country                           string `json:"country"`
	IsManualPayout                    int    `json:"is_manual_payout"`
	MembershipLevelID                 int    `json:"membership_level_id"`
	MessageVisibility                 int    `json:"message_visibility"`
	PayerEmail                        string `json:"payer_email"`
	PayerName                         string `json:"payer_name"`
	Refererr                          string `json:"referer"`
	SubscriptionCanceledOn            string `json:"subscription_canceled_on"`
	SubscriptionCoffeeNum             int    `json:"subscription_coffee_num"`
	SubscriptionCoffeePrice           string `json:"subscription_coffee_price"`
	SubscriptionCreatedOn             string `json:"subscription_created_on"`
	SubscriptionCurrency              string `json:"subscription_currency"`
	SubscriptionCurrentPeriodEnd      string `json:"subscription_current_period_end"`
	SubscriptionCurrentPeriodStart    string `json:"subscription_current_period_start"`
	SubscriptionDurationType          string `json:"subscription_duration_type"`
	SubscriptionHidden                int    `json:"subscription_hidden"`
	SubscriptionID                    int    `json:"subscription_id"`
	SubscriptionIsCanceled            int    `json:"subscription_is_canceled"`
	SubscriptionIsCanceledAtPeriodEnd bool   `json:"subscription_is_canceled_at_period_end"`
	SubscriptionMessage               string `json:"subscription_message"`
	SubscriptionUpdatedOn             string `json:"subscription_updated_on"`
	TransactionID                     string `json:"transaction_id"`
}

// Members is a slcie of BMC subscribers
type Members []*Member

type membersResponse struct {
	Total    int     `json:"total"`
	Page     int     `json:"current_page"`
	LastPage int     `json:"last_page"`
	Data     Members `json:"data"`
}

// GetMember returns a BMC subscriber by subscription ID
func (c *Client) GetMember(subscriptionID int) (*Member, error) {
	var response Member
	endpoint := fmt.Sprintf("subscriptions/%d", subscriptionID)
	err := c.Send("GET", endpoint, nil, &response)

	return &response, err
}

// GetMembers returns a slice of BMC subscribers
func (c *Client) GetMembers(status string) (Members, error) {
	var response membersResponse
	endpoint := "subscriptions?status=" + status

	err := c.Send("GET", endpoint, nil, &response)
	if err != nil {
		return nil, err
	}
	members := make(Members, 0, response.Total)
	members = append(members, response.Data...)

	for response.Page < response.LastPage {
		endpoint = fmt.Sprintf("subscriptions?status=%s&page=%d", status, response.Page+1)
		response = membersResponse{}
		err = c.Send("GET", endpoint, nil, &response)
		if err != nil {
			return nil, err
		}
		members = append(members, response.Data...)
	}

	return members, nil
}
