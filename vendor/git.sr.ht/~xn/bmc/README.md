# Golang buymeacoffee.com API client

[![Go Reference](https://pkg.go.dev/badge/git.sr.ht/~xn/bmc.svg)](https://pkg.go.dev/git.sr.ht/~xn/bmc) [BMC API reference](https://developers.buymeacoffee.com/)


## Library status

### Implemented APIs

* GET /v1/extras
* GET /v1/extras/{id}
* GET /v1/subscriptions
* GET /v1/subscriptions/{id}
* GET /v1/supporters
* GET /v1/supporters/{id}

### Tests coverage

```bash
git.sr.ht/~xn/bmc/client.go:28:		NewClient	100.0%
git.sr.ht/~xn/bmc/client.go:37:		newHTTPClient	100.0%
git.sr.ht/~xn/bmc/client.go:50:		parseError	100.0%
git.sr.ht/~xn/bmc/client.go:65:		Send		95.2%
git.sr.ht/~xn/bmc/errors.go:12:		Error		100.0%
git.sr.ht/~xn/bmc/extras.go:48:		GetExtra	100.0%
git.sr.ht/~xn/bmc/extras.go:57:		GetExtras	100.0%
git.sr.ht/~xn/bmc/members.go:44:	GetMember	100.0%
git.sr.ht/~xn/bmc/members.go:53:	GetMembers	100.0%
git.sr.ht/~xn/bmc/supporters.go:39:	GetSupporter	100.0%
git.sr.ht/~xn/bmc/supporters.go:48:	GetSupporters	100.0%
total:					(statements)	98.9%
```

check it yourself:

```bash
go test -coverprofile=bmccover.out ./...
go tool cover -func=bmccover.out
rm -f bmccover.out
```

## How To Use

### add to your project

```bash
go get git.sr.ht/~xn/bmc
```

### create a client

```go
client := bmc.NewClient(yourAPItoken)
```

### note about errors

that library provides it's own error type: `bmc.Error` with additional info, returned by bmc api.

**Error codes** (exposed as `Error.Code`):

* `0` - any library level issue (network request failed, timeout, cannot parse response, etc.)
* any other - returned by BMC API

**Error messages** (exposed as `Error.Reason`):

* error message (reason) may be set by bmc library (in case of error code `0` - any issue on library level)
* error message (reason) may be returned by BMC API (in case of error code != `0` - any issue on BMC API level)

### note about speed

BMC API uses pagination with very few elements on each page, so if you have a lot of data (`client.GetMembers()`, `client.GetSupporters()`, `client.GetExtras()`) may take a while. Don't forget to cache response!
