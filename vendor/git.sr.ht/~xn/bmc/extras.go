package bmc

import "fmt"

// Extra is a BMC extras purchase
type Extra struct {
	Extra             ExtraItem `json:"extra"`
	PayerEmail        string    `json:"payer_email"`
	PayerName         string    `json:"payer_name"`
	PurchaseAmount    string    `json:"purchase_amount"`
	PurchaseCurrency  string    `json:"purchase_currency"`
	PurchaseID        int       `json:"purchase_id"`
	PurchaseIsRevoked int       `json:"purchase_is_revoked"`
	PurchaseQuestion  string    `json:"purchase_question"`
	PurchaseUpdatedOn string    `json:"purchase_updated_on"`
	PurchasedOn       string    `json:"purchased_on"`
}

// ExtraItem is a BMC extras item
type ExtraItem struct {
	RewardID                  int    `json:"reward_id"`
	RewardTitle               string `json:"reward_title"`
	RewardDescription         string `json:"reward_description"`
	RewardConfirmationMessage string `json:"reward_confirmation_message"`
	RewardQuestion            string `json:"reward_question"`
	RewardUsed                int    `json:"reward_used"`
	RewardCreatedOn           string `json:"reward_created_on"`
	RewardUpdatedOn           string `json:"reward_updated_on"`
	RewardDeletedOn           string `json:"reward_deleted_on"`
	RewardIsActive            int    `json:"reward_is_active"`
	RewardImage               string `json:"reward_image"`
	RewardSlots               int    `json:"reward_slots"`
	RewardCoffeePrice         string `json:"reward_coffee_price"`
	RewardOrder               int    `json:"reward_order"`
}

// Extras is a slice of BMC extras purchases
type Extras []*Extra

type extrasResponse struct {
	Total    int    `json:"total"`
	Page     int    `json:"current_page"`
	LastPage int    `json:"last_page"`
	Data     Extras `json:"data"`
}

// GetExtra returns BMC extras purchase by purchase ID
func (c *Client) GetExtra(purchaseID int) (*Extra, error) {
	var response Extra
	endpoint := fmt.Sprintf("extras/%d", purchaseID)
	err := c.Send("GET", endpoint, nil, &response)

	return &response, err
}

// GetExtras returns a slice of BMC extras purchases
func (c *Client) GetExtras() (Extras, error) {
	var response extrasResponse
	endpoint := "extras"

	err := c.Send("GET", endpoint, nil, &response)
	if err != nil {
		return nil, err
	}
	extras := make(Extras, 0, response.Total)
	extras = append(extras, response.Data...)

	for response.Page < response.LastPage {
		endpoint = fmt.Sprintf("extras?page=%d", response.Page+1)
		response = extrasResponse{}
		err = c.Send("GET", endpoint, nil, &response)
		if err != nil {
			return nil, err
		}
		extras = append(extras, response.Data...)
	}

	return extras, nil
}
